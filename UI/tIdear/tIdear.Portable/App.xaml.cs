﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using tIdear.Portable;
using tIdear.Portable.RESTClient.Clients;
using tIdear.Portable.Views;
using tIdear.Views;
using tIdear.Views.Buttons;
using Xamarin.Forms;

namespace tIdear
{
	public partial class App : Application
	{
	    public static MasterDetailPage MasterDetailPage;
        public static bool IsUserLoggedIn { get; set; }

        public App ()
		{
			InitializeComponent();

		    if (!IsUserLoggedIn)
		    {
		        if (Device.RuntimePlatform == Device.iOS)
		            MainPage = new LoginPage(this);
		        else
		        {
		            MainPage = new NavigationPage(new LoginPage(this)); ;
		        }
            }
		    else
		    {
		        LoggedIn();
            }
		    
		        
        }

	    public void LoggedIn()
	    {
	        if (Device.RuntimePlatform == Device.iOS)
	            MainPage = new MainPage();
	        else
	        {
	            MasterDetailPage = new MasterDetailPage()
	            {
	                Master = new MenuPage(this),
	                Detail = new NavigationPage(new MainPage())
	            };
	            MainPage = MasterDetailPage;
	        }
        }

	    public void LoggedOut()
	    {
	        IsUserLoggedIn = false;
	        Constants.UserId = 0;
	        Constants.Username = string.Empty;
	        Constants.Password = string.Empty;

            if (Device.RuntimePlatform == Device.iOS)
	            MainPage = new LoginPage(this);
	        else
	        {
	           MainPage = new NavigationPage(new LoginPage(this)); ;
	        }
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
