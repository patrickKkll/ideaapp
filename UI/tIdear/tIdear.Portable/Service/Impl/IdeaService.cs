﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using tIdear.Models;
using tIdear.Portable.RESTClient;
using tIdear.Portable.RESTClient.Clients;
using tIdear.Service;
using Xamarin.Forms;

namespace tIdear.Portable.Service.Impl
{
    public class IdeaService: IIdeaService
    {
        private readonly IIdeaRestClient ideaRestClient;

        public IdeaService(IIdeaRestClient ideaRestClient)
        {
            this.ideaRestClient = ideaRestClient;
        }

        public async Task<ICollection<Idea>> FindAllAsync(int pageIndex= 1)
        {
            var ideas = await ideaRestClient.FindAllAsync(pageIndex);

            foreach (var idea in ideas)
            {
                if (idea.Pictures != null && idea.Pictures.Count != 0)
                {
                    IdeaPicture firstPicture = idea.Pictures.FirstOrDefault();
                    if (firstPicture != null)
                    {
                        idea.FirstPicture = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(Zipper.Unzip(firstPicture.PicturePath))));
                    }
                }
            }

            return ideas;
        }

        public async Task<ICollection<Idea>> FindMyIdeasAsync(int pageIndex = 1)
        {
            var ideas = await ideaRestClient.FindMyIdeasAsync(pageIndex);

            foreach (var idea in ideas)
            {
                if (idea.Pictures != null && idea.Pictures.Count != 0)
                {
                    IdeaPicture firstPicture = idea.Pictures.FirstOrDefault();
                    if (firstPicture != null)
                    {
                        idea.FirstPicture = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(Zipper.Unzip(firstPicture.PicturePath))));
                    }
                }
            }

            return ideas;
        }

        public async Task<ICollection<Idea>> FindLikedIdeasAsync(int pageIndex = 1)
        {
            var ideas = await ideaRestClient.FindLikedIdeasAsync(pageIndex);

            foreach (var idea in ideas)
            {
                if (idea.Pictures != null && idea.Pictures.Count != 0)
                {
                    IdeaPicture firstPicture = idea.Pictures.FirstOrDefault();
                    if (firstPicture != null)
                    {
                        idea.FirstPicture = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(Zipper.Unzip(firstPicture.PicturePath))));
                    }
                }
            }

            return ideas;
        }

        public async Task<ICollection<Idea>> FindIdeasByTag(string tagName, int pageIndex = 1)
        {
            var ideas = await ideaRestClient.FindIdeasByTag(tagName, pageIndex);

            foreach (var idea in ideas)
            {
                if (idea.Pictures != null && idea.Pictures.Count != 0)
                {
                    IdeaPicture firstPicture = idea.Pictures.FirstOrDefault();
                    if (firstPicture != null)
                    {
                        idea.FirstPicture = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(Zipper.Unzip(firstPicture.PicturePath))));
                    }
                }
            }

            return ideas;
        }

        public Task SaveAsync(Idea idea)
        {
            return ideaRestClient.SaveAsync(idea);
        }

        public Task UpdateAsync(int id, Idea idea)
        {
            idea.Pictures = new List<IdeaPicture>();
            return ideaRestClient.UpdateAsync(id, idea);
        }

        public Task EditAsync(int id, Idea idea)
        {
            return ideaRestClient.EditAsync(id, idea);
        }
    }
}
