﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ReSTServer.Models;
using tIdear.Models;
using tIdear.Portable.RESTClient;
using tIdear.Portable.RESTClient.Clients;
using tIdear.Service;

namespace tIdear.Portable.Service.Impl
{
    public class UserService : IUserService
    {
        private  IUserRestClient userRestClient;

        public UserService(IUserRestClient restClient)
        {
            userRestClient = restClient;
        } 
        public async Task<bool> LogIn(UserCredentials credentials)
        {
            var user = await userRestClient.LogIn(credentials);

            if (user != null)
            {
                Constants.Username = credentials.UserName;
                Constants.Password = credentials.Password;
                Constants.UserId = user.UserId;
                return true;
            }
            else
            {
                return false;
            }
        }

        public Task<User> GetUser(int userId)
        {
            return userRestClient.GetUser(userId);
        }

        public Task<bool> UpdateUser(int userId, User user)
        {
            return userRestClient.UpdateUser(userId, user);
        }


        public Task<ICollection<User>> GetUserBySuperlike(int ideaId)
        {
            return userRestClient.GetUserBySuperlike(ideaId);
        }
    }
}
