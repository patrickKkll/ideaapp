﻿using tIdear.Service;
using tIdear.Portable.Service;
using tIdear.Portable.Service.Impl;
using tIdear.Portable.RESTClient;
using tIdear.Portable.RESTClient.Clients;

namespace tIdear.Portable
{
    public static class Constants
    {

        public static string RestUrl = "https://10.0.2.2:44316/tidearApp/";
        //"http://192.168.0.21:3000/tidearApp/";


        public static string Username;
        public static string Password;
        public static int UserId;
        public static IUserService UserService = new UserService(new UserRestClient());
        public static IIdeaService IdeaService = new IdeaService(new IdeaRestClient());
        
    }
}
