﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace tIdear.Portable.Views
{
    public class LogoutPage : ContentPage
    {
        private App app;
        public LogoutPage(App app)
        {
            this.app = app;
        }

        protected override void OnAppearing()
        {
            //var isLoggingOut = await DisplayAlert("Logging Out", "Do you really to log out?", "Yes", "No");
            var isLoggingOut = false;
            Device.BeginInvokeOnMainThread(async () =>
            {
                isLoggingOut = await DisplayAlert("Logging Out", "Do you really want to log out?", "Yes", "No");
                if (isLoggingOut)
                {
                    app.LoggedOut();
                    await Navigation.PopAsync();
                }
            });
            
        }
    }
}
