﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using PCLCrypto;
using ReSTServer.Models;
using tIdear.Portable.Service.Impl;
using tIdear.Service;
using Xamarin.Forms;

namespace tIdear.Portable.Views
{
    class LoginPage : ContentPage
    {
        Entry usernameEntry, passwordEntry;
        Label messageLabel;
        private App app;
        bool currentlyLoggingIn = false;

        public LoginPage(App app)
        {
            this.app = app;
            Padding = new Thickness(10, 5);
            var toolbarItem = new ToolbarItem
            {
                Text = "Change IP"
            };
            toolbarItem.Clicked += OnSignUpButtonClicked;
            ToolbarItems.Add(toolbarItem);

            messageLabel = new Label();
            usernameEntry = new Entry
            {
                //Text = "Noel",
                Placeholder = "Username"
            };
            passwordEntry = new Entry
            {
                //Text = "test",
                IsPassword = true
            };
            var loginButton = new Button
            {
                Text = "Login"
            };
            loginButton.Clicked += OnLoginButtonClicked;

            Title = "Login";
            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.StartAndExpand,
                Children = {
                    new Label { Text = "Username" },
                    usernameEntry,
                    new Label { Text = "Password" },
                    passwordEntry,
                    loginButton,
                    messageLabel
                }
            };
        }

        async void OnSignUpButtonClicked(object sendere, EventArgs ee)
        {
            Entry ipAdress = new Entry()
            {
                Text = "http://192.168.0.21:3000/tidearApp/"
            };
            Button changeButton = new Button(){Text = "Change Ip"};
            changeButton.Clicked += (sender, e) =>
            {
                Constants.RestUrl = ipAdress.Text;
            };

            Navigation.PushAsync(new ContentPage()
            {
                 Content = new StackLayout()
                 {
                     Children =
                     {
                         ipAdress,
                         changeButton
                     }
                 }
            });
        }

        async void OnLoginButtonClicked(object sender, EventArgs e)
        {
            if (!currentlyLoggingIn)
            {
                currentlyLoggingIn = true;

                string pwd = passwordEntry.Text;
                string hashPwd = getSha256(pwd);
                var user = new UserCredentials
                {
                    UserName = usernameEntry.Text,
                    Password = hashPwd
                };
                bool isValid;
                try
                {
                     isValid = await Constants.UserService.LogIn(user);
                }
                catch (HttpRequestException exception)
                {
                    isValid = false;
                    Debug.WriteLine(exception.Message);
                }
                if (isValid)
                {
                    currentlyLoggingIn = false;
                    App.IsUserLoggedIn = true;
                    // Navigation.InsertPageBefore(new MainPage(), this);
                    app.LoggedIn();
                    await Navigation.PopAsync();
                }
                else
                {
                    currentlyLoggingIn = false;
                    await DisplayAlert("Wrong Login", "Username or Password is not correct. Please try again.", "OK");
                
                    passwordEntry.Text = string.Empty;
                }
               
            }
        }

        private string getSha256(string data)
        {
            byte[] byteData = Encoding.UTF8.GetBytes(data);
            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha256);
            byte[] hash = hasher.HashData(byteData);
            string hashBase64 = Convert.ToBase64String(hash);
            return hashBase64;
        }


    }
}
