﻿using Xamarin.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;
using tIdear.Models;
using tIdear.Portable;
using tIdear.Portable.Models;
using tIdear.Portable.RESTClient.Clients;
using tIdear.Portable.Service;
using tIdear.Portable.Service.Impl;
using tIdear.Portable.Views.Cards;
using Plugin.Geolocator.Abstractions;
using Plugin.Geolocator;

namespace tIdear.Views
{
    public class NewIdeaPage : ContentPage
    {
        private Entry titelTextEntry;
        private Editor summaryEditor;
        private Entry tagTextEntry;
        private RelativeLayout view;
        private Image photo;
        private ImageGallery imageGallery;
        private List<Image> images;
        private List<IdeaPicture> pictures;

        private Idea ideaToEdit;

        private Position location;
        private bool isLoaded;

        protected async override void OnAppearing()
        {
            LoadLocation();
        }

        private async void LoadLocation()
        {
            location = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(15));
            if (location != null)
            {
                isLoaded = true;
            }
        }

        public NewIdeaPage(string Title, Idea idea= null)
        {
            Padding = new Thickness(10, 5);
            this.BackgroundColor = Color.White;
            this.Title = Title;
            photo = new Image();
            pictures = new List<IdeaPicture>();
            imageGallery = new ImageGallery(200) { HeightRequest = 200 }; 
            images = new List<Image>();

            ToolbarItem saveIdea = new ToolbarItem { Text = "Save" };

            saveIdea.Clicked += Save_Clicked;
            ToolbarItems.Add(saveIdea);

            view = new RelativeLayout();

            #region ItemInit
            Label titelLabel = new Label();
            titelLabel.Text = "Idea Titel";
            titelLabel.TextColor = Color.Black;
            titelLabel.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));

            titelTextEntry = new Entry();
            titelTextEntry.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
            titelTextEntry.Placeholder = "Add a Titel";
            titelTextEntry.PlaceholderColor = Color.DarkGray;
            titelTextEntry.TextColor = Color.Black;

            Label summaryLabel = new Label();
            summaryLabel.Text = "Summary";
            summaryLabel.TextColor = Color.Black;
            summaryLabel.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));

            summaryEditor = new Editor();
            summaryEditor.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
            summaryEditor.TextColor = Color.Black;

            Label tagLabel = new Label()
            {
                Text = "Tags",
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                TextColor = Color.Black
            };

            tagTextEntry = new Entry()
            {
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                Placeholder = "Separate tags with '#'",
                TextColor = Color.Black
            };

            
            Button takePhoto = new Button();
            takePhoto.Text = "Add Picture";

            takePhoto.Clicked += takePhotto_Clicked;
            #endregion

            if (idea != null)
            {
                setValues(idea);
            }

            #region AddingToView
            view.Children.Add(titelLabel,
                Constraint.Constant(5),
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) => {
                    return parent.Width - 2;
                }),
                Constraint.Constant(Device.GetNamedSize(NamedSize.Medium, typeof(Label)) + 5));

            view.Children.Add(titelTextEntry,
                Constraint.Constant(0),
                Constraint.RelativeToView(titelLabel, (parent, sibling) => {
                    return sibling.Y + sibling.Height;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Width - 2;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Height / 10;
                }));

            view.Children.Add(summaryLabel,
                Constraint.Constant(5),
                Constraint.RelativeToView(titelTextEntry, (parent, sibling) => {
                    return sibling.Y + sibling.Height;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Width - 2;
                }),
                Constraint.Constant(Device.GetNamedSize(NamedSize.Medium, typeof(Label)) + 5));

            view.Children.Add(summaryEditor,
                Constraint.Constant(5),
                Constraint.RelativeToView(summaryLabel, (parent, sibling) => {
                    return sibling.Y + sibling.Height;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Width - 2;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Height * 3 / 10;
                }));

            view.Children.Add(tagLabel,
                Constraint.Constant(5),
                Constraint.RelativeToView(summaryEditor, (parent, sibling) => {
                    return sibling.Y + sibling.Height;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Width - 2;
                }),
                Constraint.Constant(Device.GetNamedSize(NamedSize.Medium, typeof(Label)) + 5));

            view.Children.Add(tagTextEntry,
                Constraint.Constant(5),
                Constraint.RelativeToView(tagLabel, (parent, sibling) => {
                    return sibling.Y + sibling.Height;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Width - 2;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Height / 10;
                }));

            view.Children.Add(takePhoto,
                Constraint.Constant(5),
                Constraint.RelativeToView(tagTextEntry, (parent, sibling) => {
                    return sibling.Y + sibling.Height;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Width - 2;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Height / 10;
                }));
            StackLayout imgaeStackLayout = new StackLayout
            {
                
                Children = { imageGallery}
            };
            
            view.Children.Add(imgaeStackLayout,
                Constraint.Constant(5),
                Constraint.RelativeToView(takePhoto, (parent, sibling) => {
                    return sibling.Y + sibling.Height;
                }),
                Constraint.RelativeToParent((parent) => {
                    return parent.Width-2;
                }),
                Constraint.RelativeToView(takePhoto, (parent, sibling) => {
                    return parent.Height - sibling.Y - sibling.Height;
                }));
           #endregion


            this.Content = view;
        }

        async void takePhotto_Clicked(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                DisplayAlert("Error", "Not able to pick a pictue", "OK");
                return;
            }

            var file = await CrossMedia.Current.PickPhotoAsync();

            if (file == null)
            {
                DisplayAlert("Error", "Not able to pick a pictue", "OK");
                return;
            }

            AddToPictures(file);

            photo.Source = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                return stream;
            });

            images.Add(new Image(){Source = photo.Source});
           /*var tapGesture = new TapGestureRecognizer() { NumberOfTapsRequired = 1 };
            tapGesture.Tapped += image_Clicked;
            photo.GestureRecognizers.Add(tapGesture);*/

            foreach (var image in images)
            {
                imageGallery.Children.Add(image);
            }
        }

        async Task AddToPictures(MediaFile file)
        {
            try
            {
                using (var stream = file.GetStream())
                {
                    var bytes = new byte[stream.Length];
                    await stream.ReadAsync(bytes, 0, (int) stream.Length);
                    string base64 = Convert.ToBase64String(bytes);
                    pictures.Add(new IdeaPicture(){PicturePath = Zipper.Zip(base64)});
                }
            }
            catch (IOException e)
            {
                Debug.WriteLine(e);
                Device.BeginInvokeOnMainThread(() =>
                {
                    DisplayAlert("Error", "Couldn't add picture to the idea", "OK");
                });
            }
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            string titel = titelTextEntry.Text;
            string summary = summaryEditor.Text;
            string tags = tagTextEntry.Text;
            ICollection<Tag> tagCollection = new List<Tag>();

            if (ideaToEdit ==null)
            {
                if (tags != null)
                {
                    string[] tagNames = tags.Split('#');
                    foreach (string name in tagNames)
                    {
                        var nameWithOutWhitespaces = Regex.Replace(name, @"\s+", "");
                        if (nameWithOutWhitespaces.Length != 0)
                        {
                            tagCollection.Add(new Tag() { Name = nameWithOutWhitespaces.ToLower() });
                        }
                    }
                }

                Idea idea = new Idea()
                {
                    UserId = Constants.UserId,
                    Title = titel,
                    Summary = summary,
                    CreationDate = DateTime.Now,
                    CreationPlace = isLoaded ? $"{location.Latitude}, {location.Longitude}" : "Unkown Location",
                    Dislikes = 0,
                    Likes = 0,
                    IsAccepted = false,
                    IsDeleted = false,
                    Pictures = pictures,
                    Tags = tagCollection
                };

                try
                {
                    await Constants.IdeaService.SaveAsync(idea);

                    await Navigation.PopAsync();
                }
                catch (HttpRequestException exception)
                {
                    Debug.WriteLine(exception.Message);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Error", "Oops there went something wrong... ", "OK");
                    });
                }
                
            }
            else
            {
                ICollection<Tag> tagsToSave= new List<Tag>();
                if (tags != null)
                {
                    string[] tagNames = tags.Split('#');
                    foreach (string name in tagNames)
                    {
                        bool flag = true;
                        var nameWithOutWhitespaces = Regex.Replace(name, @"\s+", "");
                        if (nameWithOutWhitespaces.Length != 0)
                        {
                            foreach (var tag in ideaToEdit.Tags)
                            {
                                if (tag.Name.Equals(nameWithOutWhitespaces))
                                {
                                    tagsToSave.Add(tag);
                                    flag = false;
                                }
                            }
                            if (flag)
                            {
                                tagsToSave.Add(new Tag{ Name = nameWithOutWhitespaces.ToLower() });
                            }
                        }
                    }
                    
                }

                ideaToEdit.Title = titel;
                ideaToEdit.Summary = summary;
                ideaToEdit.Pictures = pictures;
                ideaToEdit.Tags = tagsToSave;

                try
                {
                    await Constants.IdeaService.EditAsync(ideaToEdit.IdeaId, ideaToEdit);

                    await Navigation.PopAsync();
                }
                catch (HttpRequestException exception)
                {
                    Debug.WriteLine(exception.Message);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Error", "Oops there went something wrong... ", "OK");
                    });
                }
            }
        }

        async void image_Clicked(Object sender, EventArgs e)
        {
            int index = view.Children.IndexOf(photo);

            Image image = (Image)sender;

            Image newImage = new Image() { Source = image.Source };
            view.Children.RemoveAt(index);
            view.Children.Insert(index, newImage);
            photo = newImage;
        }

        private void setValues(Idea idea)
        {
            ideaToEdit = idea;

            titelTextEntry.Text = idea.Title;
            summaryEditor.Text = idea.Summary;
            string tags = "";
            foreach (var tag in idea.Tags)
            {
                tags += " #"+ tag.Name;
            }
            if (tags.Length >2 && tags[tags.Length-1].Equals('#'))
            {
                tags = tags.Substring(0, tags.Length - 1);
            }
            tagTextEntry.Text = tags;
            foreach (var ideaPicture in idea.Pictures)
            {
                pictures.Add(ideaPicture);
                Image image = new Image
                {
                    Source = ImageSource.FromStream(() =>
                        new MemoryStream(Convert.FromBase64String(Zipper.Unzip(ideaPicture.PicturePath))))
                };
                images.Add(image);
                imageGallery.Children.Add(image);
            }


        }
    }
}
