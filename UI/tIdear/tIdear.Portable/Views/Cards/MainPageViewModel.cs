﻿//
//  Copyright (c) 2016 MatchboxMobile
//  Licensed under The MIT License (MIT)
//  http://opensource.org/licenses/MIT
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
//  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using tIdear.Models;
using tIdear.Portable.RESTClient.Clients;
using tIdear.Portable.Service;
using tIdear.Portable.Service.Impl;
using Xamarin.Forms;

namespace tIdear.Portable.Views.Cards
{
	public class MainPageViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		List<Idea> items = new List<Idea>();
		public List<Idea> ItemsList
		{
			get	{
				return items;
			}
			set	{
				if (items == value)	{
					return;
				}
				items = value;
				OnPropertyChanged();
			}
		}
		
		protected virtual void OnPropertyChanged ([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) {
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
			
		protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
		{
			field = value;
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) {
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

	    public async Task LoadItems()
	    {
	        try
	        {
                items = (List<Idea>) await Constants.IdeaService.FindAllAsync();
	        }
	        catch (HttpRequestException e)
	        {
	            Debug.WriteLine(e.Message);
	        }
        }

	    public async Task LoadNewItems(int pageIndex)
	    {
	        try
	        {
	            var list = (List<Idea>)await Constants.IdeaService.FindAllAsync(pageIndex);
	            items.AddRange(list);
	            items = items.Distinct().ToList();
            }
	        catch (HttpRequestException e)
	        {
	            Debug.WriteLine(e.Message);
	        }
        }
	}
}

