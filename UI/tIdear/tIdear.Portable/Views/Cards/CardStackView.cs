﻿//
//  Copyright (c) 2016 MatchboxMobile
//  Licensed under The MIT License (MIT)
//  http://opensource.org/licenses/MIT
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
//  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//
using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using tIdear.Models;

namespace tIdear.Portable.Views.Cards
{
	public class CardStackView : ContentView
	{
        #region card variables
        // back card scale
        private const float BackCardScale = 1;//0.8f;
		// speed of the animations
		const int AnimLength = 250;	
			// 180 / pi
		const float DegreesToRadians = 57.2957795f; 
		// higher the number less the rotation effect
		const float CardRotationAdjuster = 0.3f; 
		// distance a card must be moved to consider to be swiped off
		public int CardMoveDistance {get; set;}

		// two cards
		const int NumCards = 2;
		CardView[] cards = new CardView[NumCards];
		// the card at the top of the stack
		int topCardIndex;
		// distance the card has been moved
		float cardDistance = 0;
		// the last items index added to the stack of the cards
		int ideaIndex = 0;
        bool ignoreTouch = false;
#endregion

	    public Idea CurrentIdea;
        // called when a card is swiped left/right with the card index in the ItemSource
        public Action<Idea> SwipedRight = null;
		public Action<Idea> SwipedLeft = null;
	    private int pageIndex;

        Label noCardLeftLabel;




        public static readonly BindableProperty ItemsSourceProperty =
			BindableProperty.Create(nameof(ItemsSource), typeof(System.Collections.IList), typeof(CardStackView), null,
    		propertyChanged: OnItemsSourcePropertyChanged);
    
		public List<Idea> ItemsSource {
			get {
				return (List<Idea>)GetValue (ItemsSourceProperty);
			}
			set {
				SetValue (ItemsSourceProperty, value);
				//ideaIndex = 0;			
			}
		}
		
		private static void OnItemsSourcePropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			((CardStackView)bindable).Setup();
		}

		public CardStackView ()
		{
		    pageIndex = 1;
			RelativeLayout view = new RelativeLayout ();
            noCardLeftLabel = new Label { Text = "No cards left to evaluate", TextColor = Color.DarkGray, FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)), HorizontalTextAlignment = TextAlignment.Center, IsVisible = false };
            view.Children.Add(noCardLeftLabel,
                    Constraint.Constant(0),
                    Constraint.RelativeToParent((parent) =>
                    {
                        return parent.Height/2;
                    }),
                    Constraint.RelativeToParent((parent) =>
                    {
                        return parent.Width;
                    }),
                    Constraint.RelativeToParent((parent) =>
                    {
                        return parent.Height;
                    }));
            // create a stack of cards
            for (int i = 0; i < NumCards; i++) {
				var card = new CardView();
				cards[i] = card;
				card.InputTransparent = true;
				card.IsVisible = false;

				view.Children.Add(
					card,
					Constraint.Constant(0),
					Constraint.Constant(0),
					Constraint.RelativeToParent((parent) =>
					{
						return parent.Width;
					}),
					Constraint.RelativeToParent((parent) =>
					{
						return parent.Height;
					})
				);
			}

			this.BackgroundColor = Color.White;
			this.Content = view;
			
			var panGesture = new PanGestureRecognizer ();
			panGesture.PanUpdated += OnPanUpdated;

            var tapGesture = new TapGestureRecognizer(){ NumberOfTapsRequired = 1};
		    tapGesture.Tapped += ShowIdeaInfo;


			GestureRecognizers.Add (panGesture);
            GestureRecognizers.Add(tapGesture);

		}
		
		void Setup()
		{
			// set the top card
		    if (ideaIndex == 0 && ItemsSource.Count != 0)
		    {
		        topCardIndex = 0;
                noCardLeftLabel.IsVisible = false;

                // create a stack of cards
		        for (int i = 0; i < Math.Min(NumCards, ItemsSource.Count); i++)
		        {
                    if (ideaIndex >= ItemsSource.Count) break;
                    
		            var card = cards[i];
		            card.Title.Text = ItemsSource[ideaIndex].Title;
		            //card.Location.Text = ItemsSource[itemIndex].Location;
		            card.Summary.Text = ItemsSource[ideaIndex].Summary;
                    
                        ImageGrid imageGallery = new ImageGrid(ItemsSource[ideaIndex].Pictures);
		                card.SetImageGallery(imageGallery);
		            

		            CurrentIdea = ItemsSource[topCardIndex];
		            card.IsVisible = true;
		            card.Scale = GetScale(i);
		            card.RotateTo(0, 0);
		            card.TranslateTo(0, -card.Y, 0);
		            ((RelativeLayout) this.Content).LowerChild(card);
		            ideaIndex++;
		        }
            }
		}
		
		void OnPanUpdated (object sender, PanUpdatedEventArgs e)
		{
			switch (e.StatusType) {
				case GestureStatus.Started:
					HandleTouchStart();
					break;
				case GestureStatus.Running:
					HandleTouch((float)e.TotalX);	
	                break;
				case GestureStatus.Completed:	
					HandleTouchEnd();
					break;
			}
		}	

		// to hande when a touch event begins
		public void HandleTouchStart() 
		{			
			cardDistance = 0;
		}

		// to handle te ongoing touch event as the card is moved
		public void HandleTouch(float diff_x) 
		{				
			if (ignoreTouch) {
				return;
			}

			var topCard = cards [topCardIndex];
			var backCard = cards [PrevCardIndex (topCardIndex)];

			// move the top card
			if (topCard.IsVisible) {

				// move the card
				topCard.TranslationX = (diff_x);

				// calculate a angle for the card
				float rotationAngel = (float)(CardRotationAdjuster * Math.Min (diff_x / this.Width, 1.0f));
				topCard.Rotation = rotationAngel * DegreesToRadians;

				// keep a record of how far its moved
				cardDistance = diff_x;
			}

			// scale the backcard
			if (backCard.IsVisible) {
				backCard.Scale = Math.Min (BackCardScale + Math.Abs ((cardDistance / CardMoveDistance) * (1.0f - BackCardScale)), 1.0f);
			}			
		}

		// to handle the end of the touch event
		public async void HandleTouchEnd()
		{			
			ignoreTouch = true;

			var topCard = cards [topCardIndex];

			// if the card was move enough to be considered swiped off
			if (Math.Abs ((int)cardDistance) > CardMoveDistance) {

				// move off the screen
				await topCard.TranslateTo (cardDistance>0?this.Width:-this.Width, 0, AnimLength/2, Easing.SpringOut);
				topCard.IsVisible = false; 
				
                if (ItemsSource.Count > 1)
                {
                    if (SwipedRight != null && cardDistance > 0)
                    {
                        SwipedRight(ItemsSource[ideaIndex - 2]);
                    }
                    else if (SwipedLeft != null)
                    {
                        SwipedLeft(ItemsSource[ideaIndex - 2]);
                    }
                    CurrentIdea = ItemsSource[ideaIndex - 2];
                }
                else
                {
                    if (SwipedRight != null && cardDistance > 0)
                    {
                        SwipedRight(ItemsSource[ideaIndex - 1]);
                    }
                    else if (SwipedLeft != null)
                    {
                        SwipedLeft(ItemsSource[ideaIndex - 1]);
                    }
                    CurrentIdea = ItemsSource[ideaIndex - 1];
                }
				
				// show the next card
				ShowNextCard ();

			}
			// put the card back in the center
			else {

				// move the top card back to the center
				topCard.TranslateTo ((-topCard.X), - topCard.Y, AnimLength, Easing.SpringOut);
				topCard.RotateTo (0, AnimLength, Easing.SpringOut);

				// scale the back card down
				var prevCard = cards [PrevCardIndex (topCardIndex)];
				await prevCard.ScaleTo(BackCardScale, AnimLength, Easing.SpringOut);

			}	

			ignoreTouch = false;
		}
			
		// show the next card
		public void ShowNextCard()
		{
			if (cards[0].IsVisible == false && cards[1].IsVisible == false) {
				Setup();
				return;
			}

            if (ideaIndex == ItemsSource.Count-5)
		    {
		        LoadNewItems();
		    }
			
			var topCard = cards [topCardIndex];
			topCardIndex = NextCardIndex (topCardIndex);
		    
		    if (ItemsSource.Count < 2)
		    {
		        CurrentIdea = ItemsSource[ideaIndex-1];
		    }
            else if (ideaIndex - 2 < ItemsSource.Count)
		    {
		        CurrentIdea = ItemsSource[ideaIndex - 2];
		    }
		    else
		    {
		        CurrentIdea = null;
		    }


            if (ItemsSource.Count == 1)
            {
                noCardLeftLabel.IsVisible = true;
            }

			// if there are more cards to show, show the next card in to place of 
			// the card that was swipped off the screen
		    if (ideaIndex < ItemsSource.Count)
		    {
		        // push it to the back z order
		        ((RelativeLayout) this.Content).LowerChild(topCard);

		        // reset its scale, opacity and rotation
		        topCard.Scale = BackCardScale;
		        topCard.RotateTo(0, 0);
		        topCard.TranslateTo(0, -topCard.Y, 0);

		        // set the data
		        topCard.Title.Text = ItemsSource[ideaIndex].Title;
		        //card.Location.Text = ItemsSource[itemIndex].Location;
		        topCard.Summary.Text = ItemsSource[ideaIndex].Summary;
                
                ImageGrid imageGallery = new ImageGrid(ItemsSource[ideaIndex].Pictures);
		        topCard.SetImageGallery(imageGallery);
                
		        


                topCard.IsVisible = true;
		        ideaIndex++;
		    }
		    else if (ideaIndex == ItemsSource.Count)
		    {
		        // reset its scale, opacity and rotation
		        topCard.Scale = BackCardScale;
		        topCard.RotateTo(0, 0);
		        topCard.TranslateTo(0, -topCard.Y, 0);

		        // set the data
		        topCard.Title.Text = ItemsSource[ideaIndex-1].Title;
		        //card.Location.Text = ItemsSource[itemIndex].Location;
		        topCard.Summary.Text = ItemsSource[ideaIndex-1].Summary;
                
		            ImageGrid imageGallery = new ImageGrid(ItemsSource[ideaIndex - 1].Pictures);
		            topCard.SetImageGallery(imageGallery);
                

                ((RelativeLayout)this.Content).LowerChild(topCard);
                topCard.IsVisible = false;
		        ideaIndex++;
		    }
		    else if (ideaIndex == ItemsSource.Count +1)
		    {
		        ((RelativeLayout)this.Content).LowerChild(topCard);
		        topCard.IsVisible = false;
		        CurrentIdea = null;
                noCardLeftLabel.IsVisible = true;
		        ideaIndex++;
		    }
		}

		// return the next card index from the top
		int NextCardIndex(int topIndex)
		{
			return topIndex == 0 ? 1 : 0;
		}

		// return the prev card index from the yop
		int PrevCardIndex(int topIndex)
		{
			return topIndex == 0 ? 1 : 0;
		}			

		// helper to get the scale based on the card index position relative to the top card
		float GetScale(int index) 
		{			
			return (index == topCardIndex) ? 1.0f : BackCardScale;
		}

        public async Task LoadNewItems()
        {
            try
            {
                var list = (List<Idea>)await Constants.IdeaService.FindAllAsync(pageIndex);
                var lastIdea = ItemsSource[ItemsSource.Count - 1];
                foreach (var idea in list)
                {
                    if (idea.IdeaId > lastIdea.IdeaId)
                    {
                        ItemsSource.Add(idea);
                    }
                }
                /*ItemsSource.AddRange(list);
                ItemsSource = ItemsSource.Distinct().ToList();*/
            }
            catch (HttpRequestException e)
            {
                Debug.WriteLine(e.Message);
            }
            
        }

	     async void ShowIdeaInfo(Object sender, EventArgs e)
	    {
            if(ItemsSource.Count == 0)
            {
                return;
            }


	        if (ItemsSource.Count < 2)
	        {
	            await Navigation.PushAsync(new IdeaInformationView(ItemsSource[ideaIndex - 1]));
	        }
	        else if (ideaIndex - 2 < ItemsSource.Count)
	        {
	            await Navigation.PushAsync(new IdeaInformationView(ItemsSource[ideaIndex - 2]));
            }
        }
    }
}

