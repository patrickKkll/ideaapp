﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tIdear.Models;
using tIdear.Portable.Service;
using Xamarin.Forms;

namespace tIdear.Portable.Views.Cards
{
    public class ImageGrid : Grid
    {
        public ICollection<IdeaPicture> pictures;
        public int rowCount = 0;

        public ImageGrid(ICollection<IdeaPicture> pictures)
        {
            if (pictures != null)
            {
                this.pictures = pictures;
                rowCount = pictures.Count % 4 == 0
                    ? pictures.Count / 4
                    : pictures.Count / 4 + 1;

                RowSpacing = 1;
                ColumnSpacing = 1;
                VerticalOptions = LayoutOptions.End;

                RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
               ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                int row = 0, column = 0;

                foreach (var picture in pictures)
                {
                    var imageSource = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(Zipper.Unzip(picture.PicturePath))));
                    Image photo = new Image { Source = imageSource };
                    if (column < 4)
                    {
                        Children.Add(photo, column, row);
                        column++;
                    }
                    else
                    {
                        if (pictures.Count > 4*(row+1))
                        {
                            RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                            column = 0;
                            row += 1;
                            Children.Add(photo, column, row);
                            column++;
                        }
                    
                    }
                }
            }
        }
    }
}
