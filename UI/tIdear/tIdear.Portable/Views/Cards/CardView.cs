﻿//
//  Copyright (c) 2016 MatchboxMobile
//  Licensed under The MIT License (MIT)
//  http://opensource.org/licenses/MIT
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
//  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

using System;
using System.Collections.Generic;
using System.IO;
using tIdear.Models;
using Xamarin.Forms;

namespace tIdear.Portable.Views.Cards
{
	public class CardView : ContentView
	{
		public Label Title { get; set;}
        public MultiLineLabel Summary { get; set; }
		public Image Photo { get; set;}

	    public ImageGrid ImageGallery { get; set; }
        //public Label Location { get; set;}
        //public Label Description { get; set;}
	    private RelativeLayout view;


        public CardView ()
		{
			view = new RelativeLayout ();
            
            BoxView border = new BoxView()
            {
                BackgroundColor = Color.Black,
                InputTransparent = true
            };
		    view.Children.Add(border, Constraint.Constant(0), Constraint.Constant(0),
		    Constraint.RelativeToParent((parent) => {
		        return parent.Width;
		    }),
		    Constraint.RelativeToParent((parent) => {
		            return parent.Height;
		        })
                );


            // box view as the background
            BoxView boxView1 = new BoxView {
				Color = Color.White,
				InputTransparent=true
			};
            view.Children.Add (boxView1,
				Constraint.Constant (1), Constraint.Constant (1),
				Constraint.RelativeToParent ((parent) => {					
					return parent.Width-2;
				}),
				Constraint.RelativeToParent ((parent) => {
					return parent.Height-2;
				})
			);		
            
            
			// items label
			Title = new Label () {				
				TextColor = Color.Black,
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
				InputTransparent=true
			};
			view.Children.Add (Title,
				Constraint.Constant (10), Constraint.Constant (10),
				Constraint.RelativeToParent ((parent) => {					
					return parent.Width -40;
				}),
				Constraint.Constant (28)
			);

		    // bottom label
            Summary = new MultiLineLabel() {
                Lines = 5,
				TextColor = Color.Black,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				HorizontalOptions=LayoutOptions.StartAndExpand,
				InputTransparent=true
			};
		    view.Children.Add(
		        Summary,
		        Constraint.Constant(10),
		        Constraint.Constant(40),
				/*Constraint.RelativeToParent ((parent) => {					
					return parent.Height - 30;
				}),*/
				Constraint.RelativeToParent ((parent) => {					
					return parent.Width - 20;
				}),
		        Constraint.RelativeToParent((parent) => {
		            return parent.Height - 40;
		        })
            );

		    ImageGallery = new ImageGrid(new List<IdeaPicture>());

		    view.Children.Add(ImageGallery,
		        Constraint.Constant(10),
		        Constraint.RelativeToParent((parent) => {
		            return Math.Max(parent.Height * 6 / 10, parent.Height - ImageGallery.pictures.Count);
		        }),
		        Constraint.RelativeToParent((parent) => {
		            return parent.Width - 20;
		        }),
		        Constraint.RelativeToParent((parent) => { return Math.Min(parent.Height * 4 / 10, ImageGallery.Height); })
		    );


            Content = view;
		}

	    public void SetImageGallery(ImageGrid imageGallery)
	    {
	        if (imageGallery.pictures != null && imageGallery.pictures.Count != 0)
	        {
	            view.Children.Remove(ImageGallery);
	            view.Children.Remove(Summary);
	            int rowsCount = imageGallery.pictures.Count % 4 == 0
	                ? imageGallery.pictures.Count / 4
	                : imageGallery.pictures.Count / 4 + 1;

	            view.Children.Add(Summary,
	                Constraint.Constant(10),
	                Constraint.Constant(40),
                    Constraint.RelativeToParent((parent) =>
	                {
	                    return parent.Width - 20;
	                }),
	                Constraint.RelativeToParent((parent) =>
	                {
	                    return parent.Height - 40 - (parent.Height * (1.5 * rowsCount) / 10);
	                })
	            );
                
                ImageGallery = imageGallery;
	            view.Children.Add(ImageGallery,
	                Constraint.Constant(10),
	                Constraint.RelativeToParent((parent) =>
	                {
	                    return (parent.Height * (10 - (rowsCount * 1.5)) / 10);
	                }),
	                Constraint.RelativeToParent((parent) =>
	                {
	                    return parent.Width - 20;
	                }),
	                Constraint.RelativeToParent((parent) => { return parent.Height * (1.5 * rowsCount) / 10; })
	            );
	            
	        }
	        else
	        {
	            view.Children.Remove(ImageGallery);
                ImageGallery = new ImageGrid(null);
	        }
	        Content = view;
        }
	}
}

