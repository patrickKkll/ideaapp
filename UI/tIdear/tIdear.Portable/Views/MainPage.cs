﻿//
//  Copyright (c) 2016 MatchboxMobile
//  Licensed under The MIT License (MIT)
//  http://opensource.org/licenses/MIT
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
//  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
//  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.CompilerServices;
using tIdear.Models;
using tIdear.Portable.RESTClient.Clients;
using tIdear.Portable.Views.Cards;
using tIdear.Portable.Service;
using tIdear.Portable.Service.Impl;
using tIdear.Views;
using Xamarin.Forms;
using Color = Xamarin.Forms.Color;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace tIdear.Portable.Views
{
	public class MainPage : ContentPage
	{				
		CardStackView cardStack;
		MainPageViewModel viewModel = new MainPageViewModel();
	    private Position location;
	    private bool isLoaded;

	    protected async override void OnAppearing()
	    {
	        LoadLocation();
	        if (cardStack.ItemsSource == null || cardStack.ItemsSource.Count == 0)
	        {
	            await viewModel.LoadItems();
	            cardStack.SetBinding(CardStackView.ItemsSourceProperty, "ItemsList");
            }
        }

	    private async void LoadLocation()
	    {
	        location = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(15));
	        if (location != null)
	        {
	            isLoaded = true;
	        }
        }

		public MainPage ()
		{

            this.BindingContext = viewModel;
			this.BackgroundColor = Color.LightGray;
		    this.Title = "Tideer";
            
            ToolbarItem addIdea = new ToolbarItem {Text = "Add"};
		   
		    addIdea.Clicked += addIdea_Clicked;
            ToolbarItems.Add(addIdea);
		   
		    RelativeLayout view = new RelativeLayout();

            #region Buttons
		    Button likeButton = new Button()
		    {
		        Text = "Like",
		        TextColor = Color.White,
		        BackgroundColor = Color.FromHex("#009374"),
		    };
		    likeButton.Clicked += likeButton_Clicked;


		    Button dislikeButton = new Button()
		    {
		        Text = "Dislike",
		        TextColor = Color.White,
		        BackgroundColor = Color.FromHex("#009374")
		    };
		    dislikeButton.Clicked += dislikeButton_Clicked;

		    Button superlikeButton = new Button()
		    {
		        Text = "Superlike",
		        TextColor = Color.White,
		        BackgroundColor = Color.FromHex("#009374")
		    };
		    superlikeButton.Clicked += superlikeButton_Clicked;

		    view.Children.Add(dislikeButton,
		        Constraint.Constant(5),
		        Constraint.RelativeToParent((parent) => {
		            return parent.Height - 45;
		        }),

		        Constraint.RelativeToParent((parent) => {
		            return parent.Width / 2 - 7.5;
		        }),
		        Constraint.Constant(40)
		    );
		    view.Children.Add(likeButton,
		        Constraint.RelativeToParent((parent) => {
		            return parent.Width / 2 + 2.5;
		        }),
		        Constraint.RelativeToParent((parent) => {
		            return parent.Height - 45;
		        }),

		        Constraint.RelativeToParent((parent) => {
		            return parent.Width / 2 - 7.5;
		        }),
		        Constraint.Constant(40)
		    );
		    view.Children.Add(superlikeButton,
		        Constraint.RelativeToParent((parent) => {
		            return parent.Width / 2 -(parent.Width / 2 - 7.5)/2;
		        }),
		        Constraint.RelativeToParent((parent) => {
		            return parent.Height - 85;
		        }),

		        Constraint.RelativeToParent((parent) => {
		            return parent.Width / 2 - 7.5;
		        }),
		        Constraint.Constant(40)
		    );


            #endregion

            cardStack = new CardStackView ();
			cardStack.SetBinding(CardStackView.ItemsSourceProperty, "ItemsList");
			cardStack.SwipedLeft += SwipedLeft;
			cardStack.SwipedRight += SwipedRight;

			view.Children.Add (cardStack,
			    Constraint.Constant(20),
			    Constraint.Constant(15),
			    Constraint.RelativeToParent((parent) => {
			        return parent.Width - 40;
			    }),
			    Constraint.RelativeToParent((parent) => {
			        return parent.Height - 100;
			    })
            );	

			this.LayoutChanged += (object sender, EventArgs e) => 
			{
				cardStack.CardMoveDistance = (int)(this.Width * 0.40f);
			};

		    

            this.Content = view;
            
		}

	    async void addIdea_Clicked(Object sender, EventArgs e)
	    {
	        await Navigation.PushAsync(new NewIdeaPage("New Idea"));
	    }

	    void likeButton_Clicked(Object sender, EventArgs e)
	    {
	        cardStack.ShowNextCard();
	        SwipedRight(cardStack.CurrentIdea);
	    }

	    void dislikeButton_Clicked(Object sender, EventArgs e)
	    {
	        cardStack.ShowNextCard();
            SwipedLeft(cardStack.CurrentIdea);
	    }

	    async void superlikeButton_Clicked(Object sender, EventArgs e)
	    {
	        cardStack.ShowNextCard();
	        var idea = cardStack.CurrentIdea;
	        if (idea != null)
	        {
	            //var location = await CrossGeolocator.Current.GetLastKnownLocationAsync();
	            UserIdeaInteraction interaction = new UserIdeaInteraction
	            {
	                IdeaId = idea.IdeaId,
	                Liked = true,
	                PlaceViewed = isLoaded ? $"{location.Latitude}, {location.Longitude}" : "Unkown Location",
	                SuperLike = true,
	                TimeSpent = new TimeSpan(0, 4, 0),
	                UserId = Constants.UserId
	            };
	            idea.Likes++;
	            idea.Interactions.Add(interaction);

	            try
	            {
	                Constants.IdeaService.UpdateAsync(idea.IdeaId, idea);
	            }
	            catch (HttpRequestException exception)
	            {
	                Debug.WriteLine(exception.Message);
	                Device.BeginInvokeOnMainThread(() =>
	                {
	                    DisplayAlert("Error", "There was a problem with the connection", "OK");
	                });
	            }
                
	        }
	    }

        async void SwipedLeft(Idea idea)
        {
            if (idea != null)
            {
                int ideaId = idea.IdeaId;
            
                UserIdeaInteraction interaction = new UserIdeaInteraction
                {
                    IdeaId = ideaId,
                    Liked = false,
                    PlaceViewed = isLoaded ? $"{location.Latitude}, {location.Longitude}" : "Unkown Location",
                    SuperLike = false,
                    TimeSpent = new TimeSpan(0,4,0),
                    UserId = Constants.UserId
                };
                idea.Dislikes++;
                idea.Interactions.Add(interaction);

                try
                {
                    Constants.IdeaService.UpdateAsync(ideaId, idea);
                }
                catch (HttpRequestException exception)
                {
                    Debug.WriteLine(exception.Message);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Error", "There was a problem with the connection", "OK");
                    });
                }
            }
           
            // card swiped to the left
        }	
		
		async void SwipedRight(Idea idea)
		{
		    if (idea != null)
		    {
		        int ideaId = idea.IdeaId;

		        //var location = await CrossGeolocator.Current.GetLastKnownLocationAsync();
		        UserIdeaInteraction interaction = new UserIdeaInteraction
		        {
		            IdeaId = ideaId,
		            Liked = true,
		            PlaceViewed = isLoaded ? $"{location.Latitude}, {location.Longitude}" : "Unkown Location",
		            SuperLike = false,
		            TimeSpent = new TimeSpan(0, 4, 0),
		            UserId = Constants.UserId
		        };
		        idea.Likes++;
		        idea.Interactions.Add(interaction);

		        try
		        {
		            Constants.IdeaService.UpdateAsync(ideaId, idea);
		        }
		        catch (HttpRequestException exception)
		        {
		            Debug.WriteLine(exception.Message);
		            Device.BeginInvokeOnMainThread(() =>
		            {
		                DisplayAlert("Error", "There was a problem with the connection", "OK");
		            });
		        }
            }
		}
	}
}


