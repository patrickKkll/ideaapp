﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using tIdear.Models;
using tIdear.Portable.Service.Impl;
using tIdear.Service;
using Xamarin.Forms;

namespace tIdear.Portable.Views
{
    public class UserInformationView : ContentPage
    {
        private User user;
        private Entry nameEntry, emailEntry, departmentEntry;

        public UserInformationView()
        {
            this.Title = "User Information";
            Padding = new Thickness(10, 5);

            ToolbarItem saveButton = new ToolbarItem { Text = "Save" };
            saveButton.Clicked += OnSaveButtonClicked;
            ToolbarItems.Add(saveButton);
        }

        public async void OnSaveButtonClicked(Object sender, EventArgs e)
        {
            user.Name = nameEntry.Text;
            user.Email = emailEntry.Text;
            user.Department = departmentEntry.Text;
            try
            {
                if (await Constants.UserService.UpdateUser(user.UserId, user))
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Successfull", "The changes saved successfully", "OK");
                    });
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Error", "Oops there went something wrong... ", "OK");
                    });
                }
            }
            catch (HttpRequestException httpRequestException)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    DisplayAlert("Error", "Oops there went something wrong... ", "OK");
                });
                Debug.WriteLine(httpRequestException.Message);
            }

        }

        protected override async void OnAppearing()
        {
            try
            {
                user = await Constants.UserService.GetUser(Constants.UserId);
            }
            catch (HttpRequestException e)
            {
                Debug.WriteLine(e.Message);
                Device.BeginInvokeOnMainThread(() =>
                {
                    DisplayAlert("Error", "Oops there went something wrong... ", "OK");
                });
            }
            if (user != null)
            {
                Label nameLabel = new Label
                {
                    Text = "Name:"
                };
                nameEntry = new Entry { Text = user.Name };

                Label emailLabel = new Label
                {
                    Text = "Email:"
                };
                emailEntry = new Entry { Text = user.Email };
                Label deparmentLabel = new Label
                {
                    Text = "Department:"
                };
                departmentEntry = new Entry { Text = user.Department };

                Content = new StackLayout
                {
                    Children =
                {
                    nameLabel,
                    nameEntry,
                    emailLabel,
                    emailEntry,
                    deparmentLabel,
                    departmentEntry
                }
                };
            }
        }
    }
}