﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using tIdear.Models;
using tIdear.Portable.RESTClient.Clients;
using tIdear.Portable.Service.Impl;
using tIdear.Service;
using Xamarin.Forms;
using HorizontalList;
using tIdear.Portable.Service;
using tIdear.Portable.Views.Cards;
using tIdear.Views;
using Xamarin.Forms.Internals;

namespace tIdear.Portable.Views
{
    public class IdeaInformationView : ContentPage
    {
        private Idea idea;
        private ICollection<User> superLikeUser;
        private Image pic;
        private StackLayout stackLayout;

        public IdeaInformationView(Idea idea, bool editIdeaFlag = false)
        {
            Padding = new Thickness(10,5);
            this.idea = idea;
            this.Title = "Idea";
            if (editIdeaFlag)
            {
                ToolbarItem editIdea = new ToolbarItem { Text = "Edit" };

                editIdea.Clicked += editIdea_Clicked;
                ToolbarItems.Add(editIdea);
            }
            
        }

        async void editIdea_Clicked(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewIdeaPage("Edit Idea", idea));
        }

        protected override async void OnAppearing()
        {
            stackLayout = new StackLayout() { Padding = new Thickness(5, 5) };

            Label titleLabel = new Label { Text = idea.Title, TextColor = Color.Black,  FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) };
            Label summaryLabel = new Label { Text = idea.Summary, TextColor = Color.Black, FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)) };

            stackLayout.Children.Add(titleLabel);
            stackLayout.Children.Add(summaryLabel);

            IdeaPicture first = idea.Pictures.FirstOrDefault();
            pic = new Image();
            if (first != null)
            {
                var picSource = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(Zipper.Unzip(first.PicturePath))));
                pic = new Image { Source = picSource };
            }

            stackLayout.Children.Add(pic);

            if (idea.Pictures.Count > 1)
            {
                double heigt = 30;
                ImageGallery imageGallery = new ImageGallery(heigt);
                imageGallery.HeightRequest = heigt;
                var tapGesture = new TapGestureRecognizer() { NumberOfTapsRequired = 1 };
                tapGesture.Tapped += image_Clicked;

                foreach (var image in idea.Pictures)
                {
                    var imageSource = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(Zipper.Unzip(image.PicturePath))));
                    Image photo = new Image
                    {
                        Source = imageSource,
                        HeightRequest = heigt
                    };
                    photo.GestureRecognizers.Add(tapGesture);
                    imageGallery.Children.Add(photo);
                }

                stackLayout.Children.Add(imageGallery);
            }

            #region tags

            if (idea.Tags.Count != 0 && idea.Tags != null)
            {
                ScrollView tagsButtons = new ScrollView
                {

                    Orientation = ScrollOrientation.Horizontal
                };

                StackLayout tagButtonsLayout = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal,
                    Padding = new Thickness(0, 0, 0, 0),
                    Margin = new Thickness(0, 0, 0, 0),
                    Spacing = 2
                };

                Label tagsLabel = new Label()
                {
                    Text = "Tags:",
                    TextColor = Color.Black,
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                };
                stackLayout.Children.Add(tagsLabel);

                foreach (var t in idea.Tags)
                {
                    Button tagButton = new Button
                    {
                        Text = $"#{t.Name}",
                        BackgroundColor = Color.DarkGray,
                        BorderRadius = 10,
                        FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                        Margin = new Thickness(0, 0, 0, 0)
                    };
                    tagButton.Clicked += tagButton_Clicked;
                    tagButtonsLayout.Children.Add(tagButton);
                }

                tagsButtons.Content = tagButtonsLayout;

                stackLayout.Children.Add(tagsButtons);
            }
            #endregion

            Label dateLabel = new Label
            {
                Text = $"Creation Date: {idea.CreationDate.ToString("g")}",
                TextColor = Color.Black,
                FontSize  = Device.GetNamedSize(NamedSize.Small, typeof(Label))
            };
            Label likeLabel = new Label
            {
                Text = $"Likes: {idea.Likes}  Dislikes: {idea.Dislikes}",
                TextColor = Color.Black,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label))
            };

            stackLayout.Children.Add(likeLabel);
            stackLayout.Children.Add(dateLabel);

            if (idea.UserId == Constants.UserId)
            {
                try
                {
                    superLikeUser = await Constants.UserService.GetUserBySuperlike(idea.IdeaId);
                }
                catch (HttpRequestException e)
                {
                    superLikeUser = new List<User>();
                    Debug.WriteLine(e.Message);
                }

                if (superLikeUser.Count != 0)
                {
                    superLikeUser.Add(new User() { Name = "Send Email to Everyone", Email = "All" });

                    ListView listView = new ListView
                    {
                        ItemsSource = superLikeUser,

                        ItemTemplate = new DataTemplate(() =>
                        {
                        // Create views with bindings for displaying each property.
                        Label nameLabel = new Label();
                            nameLabel.TextColor = Color.Black;
                            nameLabel.SetBinding(Label.TextProperty, "Name");
                            nameLabel.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));

                            Label emailLabel = new Label();
                            emailLabel.TextColor = Color.Black;
                            emailLabel.SetBinding(Label.TextProperty, "Email");
                            emailLabel.FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));

                            Label beforeEmail = new Label { Text = ": ", TextColor = Color.Black, FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label))};
                            Label afterEmail = new Label { Text = "", TextColor = Color.Black, FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label))};

                        // Return an assembled ViewCell.
                        return new ViewCell
                            {
                                View = new StackLayout
                                {
                                    Padding = new Thickness(5,5 ),
                                    Orientation = StackOrientation.Horizontal,
                                    VerticalOptions = LayoutOptions.Center,
                                    Children =
                                    {
                                    new StackLayout()
                                    {
                                        Orientation = StackOrientation.Horizontal,
                                        VerticalOptions = LayoutOptions.Start,
                                        Spacing = 0,
                                        Children =
                                        {
                                            nameLabel,
                                            beforeEmail,
                                            emailLabel,
                                            afterEmail
                                        }
                                    }
                                    }
                                }
                            };
                        })
                    };

                    listView.ItemSelected += OnSelection;


                    Label superlikeLabel = new Label { Text = "People who gave a superlike:", TextColor = Color.Black, FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)) };

                    stackLayout.Children.Add(superlikeLabel);
                    stackLayout.Children.Add(listView);

                }
            }



            ScrollView scrollView = new ScrollView()
            {
                Orientation = ScrollOrientation.Vertical,
                Content = stackLayout
            };

            Content = scrollView;
        }

        public async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            User user = (User)e.SelectedItem;
            if (user.Name.Equals("Send Email to Everyone"))
            {
                string emails = "";

                foreach (var superLiker in superLikeUser)
                {
                    if (!superLiker.Email.Equals("All"))
                    {
                        emails += superLiker.Email + ";";
                    }
                    else
                    {
                        if (emails[emails.Length-1].Equals(';'))
                        {
                            emails = emails.Substring(0, emails.Length - 1);
                        }
                    }
                }
                int firstSemiColon = emails.IndexOf(';');
                var mail = emails.Substring(0, firstSemiColon);
                var cc = "&cc=" +emails.Substring(firstSemiColon, emails.Length-firstSemiColon);
                Device.OpenUri(new Uri($"mailto:{mail}?subject={idea.Title}{cc}"));

            }
            else
            {
                Device.OpenUri(new Uri($"mailto:{user.Email}?subject={idea.Title}"));
            }
            //await Navigation.PushAsync(new IdeaInformationView((Idea)e.SelectedItem));
            //DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            //comment out if you want to keep selections
        }

        async void tagButton_Clicked(Object sender, EventArgs e)
        {
            var button = (Button)sender;
            await Navigation.PushAsync(new MyListView(button.Text));
            //DisplayAlert("Item Selected", button.Text, "Ok");
        }

        async void image_Clicked(Object sender, EventArgs e)
        {
            int index = stackLayout.Children.IndexOf(pic);

            Image image = (Image)sender;

            Image newImage = new Image() { Source = image.Source };
            stackLayout.Children.RemoveAt(index);
            stackLayout.Children.Insert(index, newImage);
            pic = newImage;
        }
    }
}