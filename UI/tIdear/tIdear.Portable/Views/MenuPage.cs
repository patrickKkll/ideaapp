﻿using System;
using System.Collections.Generic;
using System.Text;
using tIdear.Portable.RESTClient.Clients;
using tIdear.Views.Buttons;
using Xamarin.Forms;

namespace tIdear.Views
{
    class MenuPage : ContentPage
    {
        public MenuPage(App app)
        {
            var view = new StackLayout()
            {
                //Padding = new Thickness(0, Device.OnPlatform<int>(20, 0, 0), 0, 0),
                Children = {
                    new MenuLink("Ideas Overview"),
                    new MenuLink("My Liked Ideas"),
                    new MenuLink("My Ideas"),
                    new MenuLink("User Information"),
                    new MenuLink("Logout",app)
                }
            };
            Title = "Master";
            BackgroundColor = Color.White;
            Icon = null;
            Content = view;
            //Icon = Device.OS == TargetPlatform.iOS ? "menu.png" : null;
        }
    }
}
