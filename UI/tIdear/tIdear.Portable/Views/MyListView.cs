﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using tIdear.Models;
using tIdear.Portable.RESTClient.Clients;
using tIdear.Portable.Service;
using tIdear.Portable.Service.Impl;
using tIdear.Service;
using Xamarin.Forms;

namespace tIdear.Portable.Views
{
    public class MyListView : ContentPage
    {
        private ICollection<Idea> ideas;
        private ObservableCollection<Idea> items;
        private bool isLoading;
        private int pageIndex;
       
        protected override async void OnAppearing()
        {
            pageIndex = 1;
            items = new ObservableCollection<Idea>();

            try
            {
                switch (Title)
                {
                    case "My Liked Ideas":
                        ideas = await Constants.IdeaService.FindLikedIdeasAsync();
                        break;
                    case "My Ideas":
                        ideas = await Constants.IdeaService.FindMyIdeasAsync();
                        break;
                    default:
                        string tag = Title;
                        if (tag[0].Equals('#'))
                        {
                            tag = tag.Substring(1);
                        }
                        ideas = await Constants.IdeaService.FindIdeasByTag(tag, pageIndex);
                        break;
                }
            }
            catch (HttpRequestException e)
            {
                Debug.WriteLine(e.Message);
                Device.BeginInvokeOnMainThread(() =>
                {
                    DisplayAlert("Error", "Oops there went something wrong... ", "OK");
                });
                return;
            }
            
            foreach (var idea in ideas)
            {
                items.Add(idea);
            }
            ListView listView = new ListView
            {
                ItemsSource = items
            };

            listView.ItemTemplate = new DataTemplate(() =>
            {
                // Create views with bindings for displaying each property.
                Label nameLabel = new Label();
                nameLabel.TextColor = Color.Black;
                nameLabel.SetBinding(Label.TextProperty, "Title");
                nameLabel.VerticalTextAlignment = TextAlignment.Center;

                Image imageCell = new Image();
                imageCell.SetBinding(Image.SourceProperty, "FirstPicture");

                Grid cellGrid = new Grid();

                cellGrid.RowDefinitions.Add(new RowDefinition {Height = new GridLength(1, GridUnitType.Star)});
                cellGrid.ColumnDefinitions.Add(new ColumnDefinition {Width = new GridLength(1, GridUnitType.Star) });
                cellGrid.ColumnDefinitions.Add(new ColumnDefinition {Width = new GridLength(4, GridUnitType.Star)});
                cellGrid.Children.Add(imageCell, 0, 0);

                cellGrid.Children.Add(nameLabel, 1, 0);

                // Return an assembled ViewCell.
                return new ViewCell
                {
                    View = cellGrid /*new StackLayout
                        {
                            Padding = new Thickness(0, 15),
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            {
                                imageCell,
                                new StackLayout
                                {
                                    VerticalOptions = LayoutOptions.Center,
                                    Spacing = 0,
                                    Children =
                                    {
                                        nameLabel
                                        //summaryLabel
                                    }
                                }
                            }
                        }*/
                };
            });
            

            listView.ItemSelected += OnSelection;

            listView.ItemAppearing += ListView_ItemAppearing;

            Content = new StackLayout
            {
                Children =
                {
                    listView
                }
            };
        }

        private async void ListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            if (isLoading || items.Count == 0)
                return;

            //hit bottom!
            if (items.Last().Equals(e.Item))
            {
                isLoading = true;
                ICollection<Idea> list;
                pageIndex++;
                try
                {
                    switch (Title)
                    {
                    case "My Liked Ideas":
                        list  = await Constants.IdeaService.FindLikedIdeasAsync(pageIndex);
                        break;
                    case "My Ideas":
                        list = await Constants.IdeaService.FindMyIdeasAsync(pageIndex);
                        break;
                    default:
                        string tag = Title;
                        if (tag[0].Equals('#'))
                        {
                            tag = tag.Substring(1);
                        }
                        list = await Constants.IdeaService.FindIdeasByTag(tag, pageIndex);
                        break;
                    }
                }
                catch (HttpRequestException exception)
                {
                    Debug.WriteLine(exception.Message);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Error", "Couldn't load more ideas", "OK");
                    });
                    return;
                }
                
                foreach (var idea in list)
                {
                    items.Add(idea);
                }
                isLoading = false;
            }
        }

        public MyListView(string header)
        {
            this.Title = header;
        }
        
        public async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            if (Title.Equals("My Ideas"))
            {
                await Navigation.PushAsync(new IdeaInformationView((Idea)e.SelectedItem, true));
            }
            else
            {
                await Navigation.PushAsync(new IdeaInformationView((Idea)e.SelectedItem));
            }
            //DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            //comment out if you want to keep selections
        }
    }
}