﻿using System;
using System.Collections.Generic;
using System.Text;
using tIdear.Portable;
using tIdear.Portable.Views;
using Xamarin.Forms;

namespace tIdear.Views.Buttons
{
    class MenuLink : Button
    {
        public MenuLink(string name, App app = null)
        {
            Text = name;
            BackgroundColor = Color.White;
            BorderColor = Color.White;

            ContentPage newPage;

            switch (name)
            {
                case "Ideas Overview":
                    newPage = new MainPage();
                    break;
                case "My Liked Ideas":
                    newPage = new Portable.Views.MyListView(name);
                    break;
                case "My Ideas":
                    newPage = new Portable.Views.MyListView(name);
                    break;
                case "User Information":
                    newPage = new UserInformationView();
                    break;
                case "Logout":
                    //Navigation.PopAsync();
                    newPage = new LogoutPage(app);
                    break;
                default:
                    newPage = new MainPage();
                    break;
            }

            Command = new Command(o =>
            {
                App.MasterDetailPage.Detail = new NavigationPage(newPage);
                App.MasterDetailPage.IsPresented = false;
            });
        }
    }
}
