﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using tIdear.Models;

namespace tIdear.Portable.RESTClient
{
    public interface IIdeaRestClient
    {
        Task<ICollection<Idea>> FindAllAsync(int pageIndex = 1);

        Task<ICollection<Idea>> FindMyIdeasAsync(int pageIndex = 1);

        Task<ICollection<Idea>> FindLikedIdeasAsync(int pageIndex = 1);

        Task<ICollection<Idea>> FindIdeasByTag(string tagName, int pageIndex = 1);

        Task SaveAsync(Idea idea);

        Task UpdateAsync(int id, Idea idea);

        Task EditAsync(int id, Idea idea);
    }
}
