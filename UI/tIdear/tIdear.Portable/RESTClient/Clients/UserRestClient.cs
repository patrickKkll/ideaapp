﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ReSTServer.Models;
using tIdear.Models;
using tIdear.Portable.RESTClient;

namespace tIdear.Portable.RESTClient.Clients
{
    public class UserRestClient : IUserRestClient
    {
        private RestClient restClient;
        private string UserUri = "user/";

        public UserRestClient()
        {
            restClient = new RestClient();
        }

        public async Task<User> LogIn(UserCredentials credentials)
        {
            
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(30);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var json = JsonConvert.SerializeObject(credentials);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                try
                {
                    var response = await client.PostAsync(string.Concat(Constants.RestUrl, "Auth/"), content);

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<User>(responseContent);
                    }
                    else
                    {
                        throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
                    }
                }
                catch (TaskCanceledException e)
                {
                    throw new HttpRequestException(e.Message);
                }
            }
        }

        public async Task<User> GetUser(int userId)
        {
            string uri = string.Concat(UserUri, userId);

            var response = await restClient.Get(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<User>(content);
            }
            else
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }
        }

        public async Task<bool> UpdateUser(int userId, User user)
        {
            var json = JsonConvert.SerializeObject(user);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await restClient.Put(string.Concat(UserUri, userId), content);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }
        }

        public async Task<ICollection<User>> GetUserBySuperlike(int ideaId)
        {
            string uri = string.Concat(UserUri, string.Concat("ideaIdForSuperlike=", ideaId));

            var response = await restClient.Get(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ICollection<User>>(content);
            }
            else
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }
        }
    }
}
