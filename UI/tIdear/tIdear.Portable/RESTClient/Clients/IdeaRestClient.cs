﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using tIdear.Models;

namespace tIdear.Portable.RESTClient.Clients
{
    public class IdeaRestClient : IIdeaRestClient
    {
        private static string IdeaRestUri = "idea/";
        private RestClient restClient = new RestClient();

        public async Task<ICollection<Idea>> FindAllAsync(int pageIndex = 1)
        {
            ICollection<Idea> ideas = new List<Idea>();

            var uri = string.Concat(IdeaRestUri, "user=" + Constants.UserId);
            var response = await restClient.Get(uri, pageIndex);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                ideas = JsonConvert.DeserializeObject<List<Idea>>(content);
            }
            else
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }

            return ideas;
        }

        public async Task<ICollection<Idea>> FindMyIdeasAsync(int pageIndex = 1)
        {
            ICollection<Idea> ideas = new List<Idea>();

            var uri = string.Concat(IdeaRestUri, "myIdeas/" + Constants.UserId);
            var response = await restClient.Get(uri, pageIndex);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                ideas = JsonConvert.DeserializeObject<List<Idea>>(content);
            }
            else
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }

            return ideas;
        }

        public async Task<ICollection<Idea>> FindLikedIdeasAsync(int pageIndex = 1)
        {
            ICollection<Idea> ideas = new List<Idea>();

            var uri = string.Concat(IdeaRestUri, "likedIdeas/" + Constants.UserId);

            var response = await restClient.Get(uri, pageIndex);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                ideas = JsonConvert.DeserializeObject<List<Idea>>(content);
            }
            else
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }


            return ideas;
        }

        public async Task<ICollection<Idea>> FindIdeasByTag(string tagName, int pageIndex = 1)
        {
            ICollection<Idea> ideas = new List<Idea>();

            var uri = string.Concat(IdeaRestUri, $"ideaTags/{tagName}");
            var response = await restClient.Get(uri, pageIndex);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                ideas = JsonConvert.DeserializeObject<List<Idea>>(content);
            }
            else
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }


            return ideas;
        }

        public async Task SaveAsync(Idea idea)
        {
            var json = JsonConvert.SerializeObject(idea);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await restClient.Post(IdeaRestUri, content);

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }
        }

        public async Task UpdateAsync(int id, Idea idea)
        {
            var uri = string.Concat(IdeaRestUri, id);

            var json = JsonConvert.SerializeObject(idea);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await restClient.Put(uri, content);

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }
        }

        public async Task EditAsync(int id, Idea idea)
        {
            var uri = string.Concat(IdeaRestUri, $"myIdeas/{id}");

            var json = JsonConvert.SerializeObject(idea);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await restClient.Put(uri, content);

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"{(int)response.StatusCode} - {response.ReasonPhrase}");
            }
        }
    }
}
