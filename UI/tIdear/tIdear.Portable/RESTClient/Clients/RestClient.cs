﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using tIdear.Models;

namespace tIdear.Portable.RESTClient.Clients
{
    public class RestClient
    {
        public async Task<HttpResponseMessage> Get(string uri, int pageIndex = 1, int pageSize = 15)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var authData = string.Format("{0}:{1}", Constants.Username, Constants.Password);
                var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
                client.DefaultRequestHeaders.Add("PageSize", pageSize.ToString());
                client.DefaultRequestHeaders.Add("PageIndex", pageIndex.ToString());

                try
                {

                    var response = await client.GetAsync(new Uri(string.Concat(Constants.RestUrl, uri)));
                    return response;
                }
                catch (TaskCanceledException e)
                {
                    throw new HttpRequestException(e.Message);
                }
            }
        }

        public async Task<HttpResponseMessage> Post(string uri, HttpContent content)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var authData = string.Format("{0}:{1}", Constants.Username, Constants.Password);
                var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                try
                {
                    var response = await client.PostAsync(new Uri(string.Concat(Constants.RestUrl, uri)), content);
                    return response;
                }
                catch (TaskCanceledException e)
                {
                    throw new HttpRequestException(e.Message);
                }

                
            }
        }

        public async Task<HttpResponseMessage> Put(string uri, HttpContent content)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var authData = string.Format("{0}:{1}", Constants.Username, Constants.Password);
                var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                try
                {
                    var response = await client.PutAsync(new Uri(string.Concat(Constants.RestUrl, uri)), content);
                    return response;
                }
                catch (TaskCanceledException e)
                {
                    throw new HttpRequestException(e.Message);
                }
            }
        }

        public async Task<HttpResponseMessage> Delete(string uri, HttpContent content)
        {
            throw new HttpRequestException("Not implemented - not able to delete");
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var authData = string.Format("{0}:{1}", Constants.Username, Constants.Password);
                var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                var response = await client.DeleteAsync(new Uri(string.Concat(Constants.RestUrl, uri)));
                return response;
            }
        }
    }
}
