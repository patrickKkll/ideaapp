﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ReSTServer.Models;
using tIdear.Models;

namespace tIdear.Portable.RESTClient
{
    public interface IUserRestClient
    {
        Task<User> LogIn(UserCredentials credentials);

        Task<User> GetUser(int userId);

        Task<bool> UpdateUser(int userId, User user);

        Task<ICollection<User>> GetUserBySuperlike(int ideaId);
    }
}
