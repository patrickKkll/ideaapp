﻿using System;
using System.Collections.Generic;

namespace tIdear.Models
{
    public class IdeaMemo
    {
        public int MemoId { get; set; }
        
        public int IdeaId { get; set; }

        public string MemoPath { get; set; }

    }
}