﻿using System;
using System.Collections.Generic;

namespace tIdear.Models
{
    public class IdeaPicture
    {

        public int PictureId { get; set; }

        public int IdeaId { get; set; }

        public string PicturePath { get; set; }

    }
}