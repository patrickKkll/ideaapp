﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tIdear.Models;

namespace tIdear.Portable.Models
{
    public class Tag
    {
        public int TagId { get; set; }

        public string Name { get; set; }
    }
}
