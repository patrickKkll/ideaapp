﻿using System;
using System.Collections.Generic;

namespace tIdear.Models
{
    public class User
    {
        public int UserId { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public Gender Gender { get; set; }

        public string Department { get; set; }

        public UserRights UserRights { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<UserIdeaInteraction> UserIdeaInteractions { get; set; }

        public ICollection<Idea> Ideas { get; set; }
    }

    public enum Gender
    {
        Female,
        Male
    }

    public enum UserRights
    {
        User,
        Admin
    }
}