﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using tIdear.Portable.Models;
using Xamarin.Forms;

namespace tIdear.Models
{
	public class Idea
	{
	    public int IdeaId { get; set; }

	    public int UserId { get; set; }

	    public string Title { get; set; }

	    public string Summary { get; set; }

	    public int Likes { get; set; }

	    public int Superlikes { get; set; }

        public int Dislikes { get; set; }

	    public DateTime CreationDate { get; set; }

        public string CreationPlace { get; set; }

        public bool IsAccepted { get; set; }

	    public bool IsDeleted { get; set; }

	    public ICollection<IdeaMemo> Memos { get; set; }
	    
        public ICollection<IdeaPicture> Pictures { get; set; }

        [JsonIgnore]
        public ImageSource FirstPicture { get; set; }

	    public ICollection<UserIdeaInteraction> Interactions { get; set; }

        public ICollection<Tag> Tags { get; set; }

	    protected bool Equals(Idea other)
	    {
	        return IdeaId == other.IdeaId && UserId == other.UserId && string.Equals(Title, other.Title) && string.Equals(Summary, other.Summary) && Likes == other.Likes && Superlikes == other.Superlikes && Dislikes == other.Dislikes && CreationDate.Equals(other.CreationDate) && IsAccepted == other.IsAccepted && IsDeleted == other.IsDeleted;
	    }

	    public override bool Equals(object obj)
	    {
	        if (ReferenceEquals(null, obj)) return false;
	        if (ReferenceEquals(this, obj)) return true;
	        if (obj.GetType() != this.GetType()) return false;
	        return Equals((Idea) obj);
	    }

	    public override int GetHashCode()
	    {
	        unchecked
	        {
	            var hashCode = IdeaId;
	            hashCode = (hashCode * 397) ^ UserId;
	            hashCode = (hashCode * 397) ^ (Title != null ? Title.GetHashCode() : 0);
	            hashCode = (hashCode * 397) ^ (Summary != null ? Summary.GetHashCode() : 0);
	            hashCode = (hashCode * 397) ^ Likes;
	            hashCode = (hashCode * 397) ^ Superlikes;
	            hashCode = (hashCode * 397) ^ Dislikes;
	            hashCode = (hashCode * 397) ^ CreationDate.GetHashCode();
	            hashCode = (hashCode * 397) ^ IsAccepted.GetHashCode();
	            hashCode = (hashCode * 397) ^ IsDeleted.GetHashCode();
	            return hashCode;
	        }
	    }
	}
}