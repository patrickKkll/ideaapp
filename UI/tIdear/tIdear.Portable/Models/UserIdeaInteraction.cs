﻿using System;

namespace tIdear.Models
{
    public class UserIdeaInteraction
    {
        public int InteractionId { get; set; }
        
        public int UserId { get; set; }
        
        public int IdeaId { get; set; }

        public TimeSpan TimeSpent { get; set; }

        public DateTime InteractionDate { get; set; }

        public bool Liked { get; set; }

        public bool SuperLike { get; set; }

        public string PlaceViewed { get; set; }
    }
}