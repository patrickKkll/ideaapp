﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using tIdear.Droid;
using tIdear.Portable.Views.Cards;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(MultiLineLabel), typeof(MultiLineLabelRendererDroid))]
namespace tIdear.Droid
{
    public class MultiLineLabelRendererDroid : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            var baseLabel = (MultiLineLabel)Element;

            Control.SetLines(baseLabel.Lines);
            Control.Ellipsize = TextUtils.TruncateAt.End;
        }
    }
}