﻿using tIdear.iOS;
using tIdear.Portable.Views.Cards;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(MultiLineLabel), typeof(MultiLineLabelRendererIOS))]
namespace tIdear.iOS
{
    public class MultiLineLabelRendererIOS : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            var baseLabel = (MultiLineLabel)this.Element;

            Control.Lines = baseLabel.Lines;
        }

    }
}