namespace ReSTServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IdeaMemoes",
                c => new
                    {
                        MemoId = c.Int(nullable: false, identity: true),
                        IdeaId = c.Int(nullable: false),
                        MemoPath = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.MemoId)
                .ForeignKey("dbo.Ideas", t => t.IdeaId, cascadeDelete: true)
                .Index(t => t.IdeaId);
            
            CreateTable(
                "dbo.Ideas",
                c => new
                    {
                        IdeaId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Title = c.String(unicode: false),
                        Summary = c.String(unicode: false),
                        Likes = c.Int(nullable: false),
                        Dislikes = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false, precision: 0),
                        IsAccepted = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdeaId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserIdeaInteractions",
                c => new
                    {
                        InteractionId = c.Int(nullable: false, identity: true),
                        TimeSpent = c.Time(nullable: false, precision: 0),
                        Liked = c.Boolean(nullable: false),
                        PlaceViewed = c.String(unicode: false),
                        UserId = c.Int(nullable: false),
                        IdeaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InteractionId)
                .ForeignKey("dbo.Ideas", t => t.IdeaId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.IdeaId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Email = c.String(unicode: false),
                        Name = c.String(unicode: false),
                        Department = c.String(unicode: false),
                        UserRights = c.String(unicode: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.UserCredentials",
                c => new
                    {
                        UserCredentialsId = c.Int(nullable: false),
                        UserName = c.String(unicode: false),
                        Password = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.UserCredentialsId)
                .ForeignKey("dbo.Users", t => t.UserCredentialsId)
                .Index(t => t.UserCredentialsId);
            
            CreateTable(
                "dbo.IdeaPictures",
                c => new
                    {
                        PictureId = c.Int(nullable: false, identity: true),
                        IdeaId = c.Int(nullable: false),
                        PicturePath = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.PictureId)
                .ForeignKey("dbo.Ideas", t => t.IdeaId, cascadeDelete: true)
                .Index(t => t.IdeaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IdeaPictures", "IdeaId", "dbo.Ideas");
            DropForeignKey("dbo.IdeaMemoes", "IdeaId", "dbo.Ideas");
            DropForeignKey("dbo.UserIdeaInteractions", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserCredentials", "UserCredentialsId", "dbo.Users");
            DropForeignKey("dbo.Ideas", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserIdeaInteractions", "IdeaId", "dbo.Ideas");
            DropIndex("dbo.IdeaPictures", new[] { "IdeaId" });
            DropIndex("dbo.UserCredentials", new[] { "UserCredentialsId" });
            DropIndex("dbo.UserIdeaInteractions", new[] { "IdeaId" });
            DropIndex("dbo.UserIdeaInteractions", new[] { "UserId" });
            DropIndex("dbo.Ideas", new[] { "UserId" });
            DropIndex("dbo.IdeaMemoes", new[] { "IdeaId" });
            DropTable("dbo.IdeaPictures");
            DropTable("dbo.UserCredentials");
            DropTable("dbo.Users");
            DropTable("dbo.UserIdeaInteractions");
            DropTable("dbo.Ideas");
            DropTable("dbo.IdeaMemoes");
        }
    }
}
