namespace ReSTServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdeaCreationPlace : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ideas", "CreationPlace", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ideas", "CreationPlace");
        }
    }
}
