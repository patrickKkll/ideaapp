namespace ReSTServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tags_and_IdeaChanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagId = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.TagId);
            
            CreateTable(
                "dbo.TagIdeas",
                c => new
                    {
                        Tag_TagId = c.Int(nullable: false),
                        Idea_IdeaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagId, t.Idea_IdeaId })
                .ForeignKey("dbo.Tags", t => t.Tag_TagId, cascadeDelete: true)
                .ForeignKey("dbo.Ideas", t => t.Idea_IdeaId, cascadeDelete: true)
                .Index(t => t.Tag_TagId)
                .Index(t => t.Idea_IdeaId);
            
            AddColumn("dbo.Ideas", "Superlikes", c => c.Int(nullable: false));
            AddColumn("dbo.UserIdeaInteractions", "InteractionDate", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Users", "Gender", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "UserRights", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagIdeas", "Idea_IdeaId", "dbo.Ideas");
            DropForeignKey("dbo.TagIdeas", "Tag_TagId", "dbo.Tags");
            DropIndex("dbo.TagIdeas", new[] { "Idea_IdeaId" });
            DropIndex("dbo.TagIdeas", new[] { "Tag_TagId" });
            AlterColumn("dbo.Users", "UserRights", c => c.String(unicode: false));
            DropColumn("dbo.Users", "Gender");
            DropColumn("dbo.UserIdeaInteractions", "InteractionDate");
            DropColumn("dbo.Ideas", "Superlikes");
            DropTable("dbo.TagIdeas");
            DropTable("dbo.Tags");
        }
    }
}
