namespace ReSTServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SuperLike_Added : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserIdeaInteractions", "SuperLike", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserIdeaInteractions", "SuperLike");
        }
    }
}
