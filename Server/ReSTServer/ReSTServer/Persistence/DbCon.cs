using ReSTServer.Models;
using System;
using System.Data.Entity;
using System.Linq;
using MySql.Data.Entity;


namespace ReSTServer.Persistence

{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class DbCon : DbContext
    {
        public DbCon()
            : base("TidearDatabase")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Idea> Ideas { get; set; }
        public DbSet<IdeaMemo> IdeaMemos { get; set; }
        public DbSet<IdeaPicture> IdeaPictures { get; set; }
        public DbSet<UserIdeaInteraction> UserIdeaInteractions { get; set; }
        public DbSet<UserCredentials> UserCredentialses { get; set; }
        public DbSet<Tag> Tags { get; set; }
     
    }
    
}