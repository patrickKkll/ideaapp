﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReSTServer.Models;

namespace ReSTServer.Persistence
{
    public interface IUserCredentialsRepo
    {
        bool Authenticate(string username, string pwd);

        User LogIn(string user, string pwd);
    }
}