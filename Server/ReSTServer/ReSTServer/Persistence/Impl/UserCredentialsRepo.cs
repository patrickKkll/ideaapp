﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using ReSTServer.Models;

namespace ReSTServer.Persistence.Impl
{
    public class UserCredentialsRepo : IUserCredentialsRepo
    {
        public bool Authenticate(string username, string pwd)
        {
            using (var dbContext = new DbCon())
            {
                UserCredentials tmp = dbContext.UserCredentialses.FirstOrDefault(u => u.UserName == username);

                if (tmp != null )
                {
                    var salt = tmp.Salt;
                    
                    var hashPwd = getSha256(pwd + Convert.ToBase64String(salt));

                    if (tmp.Password == hashPwd)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                    
                    
                }
                else
                {
                    return false;
                }
            }
        }

        private string getSha256(string data)
        {
            byte[] byteData = Encoding.UTF8.GetBytes(data);
            SHA256 hasher = new SHA256Managed();
            byte[] hash = hasher.ComputeHash(byteData);
            string hashBase64 = Convert.ToBase64String(hash);
            return hashBase64;
        }

        public User LogIn(string username, string pwd)
        {
            using (var dbContext = new DbCon())
            {
                UserCredentials tmp = dbContext.UserCredentialses.Include("User").FirstOrDefault(u => u.UserName == username);

                if (tmp != null)
                {
                    var salt = tmp.Salt;
                    if (salt == null || salt.Length == 0)
                    {
                        salt = new byte[32];
                        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                        rng.GetNonZeroBytes(salt);
                        var pwdHashWithSalt = getSha256(pwd + Convert.ToBase64String(salt));
                        tmp.Salt = salt;
                        tmp.Password = pwdHashWithSalt;
                        dbContext.SaveChangesAsync();
                    }
                    var hashPwd = getSha256(pwd+Convert.ToBase64String(salt));

                    if (tmp.Password == hashPwd)
                    {
                        return tmp.User;
                    }
                    else
                    {
                        return null;
                    }


                }
                else
                {
                    return null;
                }

                
            }
        }
    }
}