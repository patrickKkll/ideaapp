﻿using ReSTServer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq.Expressions;
using System.Linq;
using System.Web;

namespace ReSTServer.Persistence.Impl
{
    public class IdeaRepo: IIdeaRepo
    {
        public ICollection<Idea> FindAll()
        {
            ICollection<Idea> ideas = new List<Idea>();

            using (var dbContext = new DbCon())
            {
                ideas = dbContext.Ideas
                    .Include(idea => idea.User.UserCredentials)
                    .Include(idea => idea.Interactions)
                    .Include(idea => idea.Pictures)
                    .Include(idea => idea.Memos)
                    .Include(idea => idea.Tags)
                    .OrderBy(idea => idea.IdeaId)
                    .ToList();
                //ideas = dbContext.Ideas.Include(idea => idea.User .Select(user => user.UserCredentials)).ToList();
                //ideas = (from idea in dbContext.Ideas.Include("User").Include("IdeaMemo").Include("IdeaPicture") join user in dbContext.Users.Include("UserCredentials") on idea.User.UserId equals user.UserId select idea ).ToList();
            }
            return ideas;
        }

        public ICollection<Idea> FindAllByUser(int userId, int pageIndex, int pageSize)
        {
            ICollection<Idea> ideas = new List<Idea>();

            using (var dbContext = new DbCon())
            {
                var interactions = dbContext.UserIdeaInteractions
                    .Where(i => i.UserId == userId)
                    .Select(i => i.IdeaId);

                ideas = dbContext.Ideas
                    .Include(idea => idea.User.UserCredentials)
                    .Include(idea => idea.Interactions)
                    .Include(idea => idea.Pictures)
                    .Include(idea => idea.Memos)
                    .Include(idea => idea.Tags)
                    .Where(idea => !interactions.Contains(idea.IdeaId))
                    .Where(idea => idea.UserId != userId)
                    .OrderBy(idea => idea.IdeaId)
                    .Skip((pageIndex-1)*pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return ideas;
        }

        public ICollection<Idea> FindAllLikedByUser(int userId, int pageIndex, int pageSize)
        {
            ICollection<Idea> ideas = new List<Idea>();

            using (var dbContext = new DbCon())
            {
                var interactions = dbContext.UserIdeaInteractions
                    .Where(i => i.UserId == userId)
                    .Where(i => i.Liked == true)
                    .Select(i => i.IdeaId);

                ideas = dbContext.Ideas
                    .Include(idea => idea.User.UserCredentials)
                    .Include(idea => idea.Interactions)
                    .Include(idea => idea.Pictures)
                    .Include(idea => idea.Memos)
                    .Include(idea => idea.Tags)
                    .Where(idea => interactions.Contains(idea.IdeaId))
                    .Where(idea => idea.UserId != userId)
                    .OrderByDescending(idea => idea.IdeaId)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return ideas;
        }

        public ICollection<Idea> FindAllFromUser(int userId, int pageIndex, int pageSize)
        {
            ICollection<Idea> ideas = new List<Idea>();

            using (var dbContext = new DbCon())
            {
               ideas = dbContext.Ideas
                    .Include(idea => idea.User.UserCredentials)
                    .Include(idea => idea.Interactions)
                    .Include(idea => idea.Pictures)
                    .Include(idea => idea.Memos)
                    .Include(idea => idea.Tags)
                    .Where(idea => idea.UserId == userId)
                    .OrderBy(idea => idea.IdeaId)
                   .Skip((pageIndex - 1) * pageSize)
                   .Take(pageSize)
                    .ToList();
            }
            return ideas;
        }

        public ICollection<Idea> FindAllByTag(string tagName, int pageIndex, int pageSize)
        {
            ICollection<Idea> ideas = new List<Idea>();
            using (var dbContext = new DbCon())
            {
                
                var tmp = dbContext.Tags
                    .Include(t => t.Ideas)
                    .FirstOrDefault(t => t.Name.Equals(tagName));

                /*IList<Idea> tmpIdea = (IList<Idea>) tmp.Ideas;

                for (int i = (pageIndex-1)*pageSize; i < tmp.Ideas.Count && i < pageIndex * pageSize; i++)
                {
                    ideas.Add(dbContext.Ideas
                        .Include(idea => idea.User.UserCredentials)
                        .Include(idea => idea.Interactions)
                        .Include(idea => idea.Pictures)
                        .Include(idea => idea.Memos)
                        .Include(idea => idea.Tags)
                        .Where(idea => tmpIdea[i].IdeaId == idea.IdeaId)
                        .OrderBy(idea => idea.IdeaId)
                        .FirstOrDefault());
                }*/

                if (tmp != null)
                {
                    using (var list = tmp.Ideas.GetEnumerator())
                    {
                        list.MoveNext();
                        for (int i = 0; i < tmp.Ideas.Count && i < pageIndex * pageSize; i++)
                        {
                            if (i < (pageIndex - 1) * pageSize)
                            {
                                list.MoveNext();
                            }
                            else
                            {
                                if (list.Current != null)
                                {
                                    var ideaId = list.Current.IdeaId;
                                    ideas.Add(dbContext.Ideas
                                        .Include(idea => idea.User.UserCredentials)
                                        .Include(idea => idea.Interactions)
                                        .Include(idea => idea.Pictures)
                                        .Include(idea => idea.Memos)
                                        .Include(idea => idea.Tags)
                                        .Where(idea => ideaId == idea.IdeaId)
                                        .OrderBy(idea => idea.IdeaId)
                                        .FirstOrDefault());

                                    list.MoveNext();
                                }
                            }
                        }
                    }
                }
            }
            return ideas;
        }

        public Idea FindOne(int id)
        {
            using (var dbContext = new DbCon())
            {
                Idea tmp = dbContext.Ideas
                    .Include(idea => idea.User.UserCredentials)
                    .Include(idea => idea.Interactions)
                    .Include(idea => idea.Pictures)
                    .Include(idea => idea.Memos)
                    .Include(idea => idea.Tags)
                    .FirstOrDefault(idea => idea.IdeaId == id);
                if (tmp != null)
                {
                    return tmp;
                }
                else
                {
                    throw new DataException($"No idea with ID = {id} found");
                }
            }
        }

        public void Save(ICollection<Idea> ideas)
        {
            using (var dbContext = new DbCon())
            {
                foreach (Idea idea in ideas)
                {
                    dbContext.Ideas.Add(idea);
                }
                dbContext.SaveChangesAsync();
            }
        }

        public void Save(Idea idea)
        {
            using (var dbContext = new DbCon())
            {
                dbContext.Ideas.Add(idea);
                dbContext.SaveChangesAsync();
            }
        }

        public void Update(int id, Idea idea)
        {
            using (var dbContext = new DbCon())
            {
                var tmp = dbContext.Ideas
                    .Include(i => i.User.UserCredentials)
                    .Include(i => i.Interactions)
                    .Include(i => i.Pictures)
                    .Include(i => i.Memos)
                    .Include(i => i.Tags)
                    .FirstOrDefault(i => i.IdeaId == id);

                if (tmp != null && tmp.IdeaId == idea.IdeaId)
                {
                    tmp.Summary = idea.Summary;
                    tmp.Title = idea.Title;
                    tmp.Likes = idea.Likes;
                    tmp.Dislikes = idea.Dislikes;
                    tmp.IsAccepted = idea.IsAccepted;
                    
                    foreach (var interaction in idea.Interactions)
                    {
                        if (!tmp.Interactions.Contains(interaction) && interaction.InteractionId == 0)
                        {
                            dbContext.Entry(interaction).State = EntityState.Added;
                        }
                    }
                    
                    dbContext.SaveChanges(); 
                }
                else
                {
                    throw new DataException($"No idea with ID = {id} found");
                }
                
            }
        }

        public void Edit(int id, Idea idea)
        {
            using (var dbContext = new DbCon())
            {
                var tmp = dbContext.Ideas
                    .Include(i => i.User.UserCredentials)
                    .Include(i => i.Interactions)
                    .Include(i => i.Pictures)
                    .Include(i => i.Memos)
                    .Include(i => i.Tags)
                    .FirstOrDefault(i => i.IdeaId == id);

                if (tmp != null && tmp.IdeaId == idea.IdeaId)
                {
                    tmp.Summary = idea.Summary;
                    tmp.Title = idea.Title;
                    tmp.Likes = idea.Likes;
                    tmp.Dislikes = idea.Dislikes;
                    tmp.IsAccepted = idea.IsAccepted;

                    foreach (var interaction in idea.Interactions)
                    {
                        if (!tmp.Interactions.Contains(interaction) && interaction.InteractionId == 0)
                        {
                            dbContext.Entry(interaction).State = EntityState.Added;
                        }
                    }

                    foreach (var picture in idea.Pictures)
                    {
                        if (!tmp.Pictures.Contains(picture) && picture.PictureId == 0)
                        {
                             dbContext.Entry(picture).State = EntityState.Added;
                        }
                    }

                    foreach (var tag in idea.Tags)
                    {
                        var dbTag = dbContext.Tags.Include(t => t.Ideas).FirstOrDefault(t => t.Name.Equals(tag.Name));

                        if (!tmp.Tags.Contains(tag) && tag.TagId == 0)
                        {
                            if (tag.TagId == 0 && dbTag ==null)
                            {
                                dbContext.Entry(tag).State = EntityState.Added;
                                tmp.Tags.Add(tag);
                            }
                            else
                            {
                                tmp.Tags.Add(dbTag);
                            }
                        }
                    }

                    dbContext.SaveChanges();
                }
                else
                {
                    throw new DataException($"No idea with ID = {id} found");
                }

            }
        }

        public int HighestIdeaId()
        {
            using (var dbContext = new DbCon())
            {
                return dbContext.Ideas.Count();
            }
        }

        public int HighestPictureId()
        {
            using (var dbContext = new DbCon())
            {
                return dbContext.IdeaPictures.Count();
            }
        }
    }
}