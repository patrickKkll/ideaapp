﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using ReSTServer.Models;

namespace ReSTServer.Persistence.Impl
{
    public class UserRepo : IUserRepo
    {
        public ICollection<User> FindAll()
        {
            ICollection<User> users = new List<User>();
            
            using (var dbContext = new DbCon())
            {
                //dbContext.Users.Include("UserCredentials");
                //users = (from user in dbContext.Users.Include("Ideas").Include("UserCredentials").Include("UserIdeaInteractions") //join usercredential in dbContext.UserCredentialses on user.UserId equals usercredential.User. select user).ToList();
                users = dbContext.Users
                    .Include(user => user.UserCredentials)
                    .Include(user => user.UserIdeaInteractions)
                    .Include(user => user.Ideas)
                    .OrderBy(user => user.UserId)
                    .ToList();
            }
            return users;
        }

        public ICollection<User> GetForSuperlike(int ideaId)
        {
            ICollection<User> users = new List<User>();

            using (var dbContext = new DbCon())
            {
                var interactions = dbContext.UserIdeaInteractions
                    .Where(i => i.IdeaId == ideaId)
                    .Where(i => i.SuperLike == true)
                    .Select(i => i.UserId);

                //dbContext.Users.Include("UserCredentials");
                //users = (from user in dbContext.Users.Include("Ideas").Include("UserCredentials").Include("UserIdeaInteractions") //join usercredential in dbContext.UserCredentialses on user.UserId equals usercredential.User. select user).ToList();
                users = dbContext.Users
                    .Include(user => user.UserCredentials)
                    .Include(user => user.UserIdeaInteractions)
                    .Include(user => user.Ideas)
                    .OrderBy(user => user.UserId)
                    .Where(user => interactions.Contains(user.UserId))
                    .ToList();
            }
            return users;
        }
        public void Save(ICollection<User> users)
        {
            using (var dbContext = new DbCon())
            {
                foreach (var user in users)
                {
                    dbContext.Users.Add(user);
                }
                dbContext.SaveChangesAsync();
            }
        }

        public void Save(User user)
        {
            using (var dbContext = new DbCon())
            {
                dbContext.Users.Add(user);
                dbContext.SaveChangesAsync();
            }
        }

        public void Update(int id, User user)
        {
            using (var dbContext = new DbCon())
            {
                User tmp = dbContext.Users.Find(id);
                if (tmp != null && tmp.UserId == user.UserId)
                {
                    tmp.Name = user.Name;
                    tmp.Email = user.Email;
                    tmp.Department = user.Department;
                    tmp.IsDeleted = user.IsDeleted;
                    tmp.UserRights = user.UserRights;
                    dbContext.SaveChangesAsync();
                }
                else
                {
                    throw new DataException($"No user with ID = {id} found");
                }
            }
        }

        public User FindOne(int id)
        {
            using (var dbContext = new DbCon())
            {
                User tmp = dbContext.Users.Find(id);
                if (tmp != null)
                {
                    return tmp;
                }
                else
                {
                    throw new DataException($"No user with ID = {id} found");
                }
            }
        }
    }
}