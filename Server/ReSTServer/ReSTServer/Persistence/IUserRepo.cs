﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReSTServer.Models;

namespace ReSTServer.Persistence
{
    public interface IUserRepo
    {
        ICollection<User> FindAll();

        ICollection<User> GetForSuperlike(int ideaId);

        void Save(ICollection<User> users);

        void Save(User user);

        void Update(int id, User user);

        User FindOne(int id);
    }
}