﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReSTServer.Models;

namespace ReSTServer.Persistence
{
    public interface IIdeaRepo
    {
        ICollection<Idea> FindAll();

        ICollection<Idea> FindAllByUser(int userId, int pageIndex, int pageSize);

        ICollection<Idea> FindAllLikedByUser(int userId, int pageIndex, int pageSize);

        ICollection<Idea> FindAllFromUser(int userId, int pageIndex, int pageSize);

        ICollection<Idea> FindAllByTag(string tagName, int pageIndex, int pageSize);

        void Save(ICollection<Idea> ideas);

        void Save(Idea idea);

        void Update(int id, Idea idea);

        void Edit(int id, Idea idea);

        Idea FindOne(int id);

        int HighestIdeaId();

        int HighestPictureId();
    }
}