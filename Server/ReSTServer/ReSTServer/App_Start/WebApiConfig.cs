﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ReSTServer
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web-API-Konfiguration und -Dienste

            // Web-API-Routen
            config.MapHttpAttributeRoutes();
            
            config.Routes.MapHttpRoute(
                name: "TidearApi",
                routeTemplate: "tidearApp/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(name: "TidearApiWithAction",
                routeTemplate: "tidearApp/{controller}/{action}/{id}",
                defaults: new {id = RouteParameter.Optional});
        }
    }
}
