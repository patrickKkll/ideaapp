﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReSTServer.Models;

namespace ReSTServer.Services
{
    public interface IAuthService
    {
        bool Authenticate(string user, string pwd);

        User LogIn(string user, string pwd);
    }
}