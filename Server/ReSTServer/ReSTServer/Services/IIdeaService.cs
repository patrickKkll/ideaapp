﻿using System.Collections.Generic;
using ReSTServer.Models;

namespace ReSTServer.Services
{
    public interface IIdeaService
    {
        ICollection<Idea> FindAll();

        ICollection<Idea> FindAllByUser(int userId, int pageIndex, int pageSize);

        ICollection<Idea> FindAllLikedByUser(int userId, int pageIndex, int pageSize);

        ICollection<Idea> FindAllFromUser(int userId, int pageIndex, int pageSize);

        ICollection<Idea> FindAllByTag(string tagName, int pageIndex, int pageSize);

        Idea FindOne(int id);

        void Save(Idea idea);

        void Update(int id, Idea idea);

        void Edit(int id, Idea idea);
    }
}