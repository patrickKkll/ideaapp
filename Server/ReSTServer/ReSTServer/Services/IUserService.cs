﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReSTServer.Models;

namespace ReSTServer.Services
{
    public interface IUserService
    {
        ICollection<User> FindAll();

        User FindOne(int id);

        ICollection<User> GetForSuperlike(int ideaId);

        void Save(User user);

        void Update(int id, User user);
    }
}