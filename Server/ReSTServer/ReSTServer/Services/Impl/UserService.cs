﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReSTServer.Models;
using ReSTServer.Persistence;

namespace ReSTServer.Services.Impl
{
    public class UserService : IUserService
    {
        private IUserRepo userRepo;

        public UserService(IUserRepo userRepo)
        {
            this.userRepo = userRepo;
        }

        public ICollection<User> FindAll()
        {
            return userRepo.FindAll();
        }

        public ICollection<User> GetForSuperlike(int ideaId)
        {
            return userRepo.GetForSuperlike(ideaId);
        }

        public User FindOne(int id)
        {
            return userRepo.FindOne(id);
        }

        public void Save(User user)
        {
           userRepo.Save(user);
        }

        public void Update(int id, User user)
        {
            userRepo.Update(id, user);
        }
    }
}