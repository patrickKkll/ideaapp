﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ReSTServer.Models;
using ReSTServer.Persistence;

namespace ReSTServer.Services
{
    public class IdeaService : IIdeaService
    {
        private IIdeaRepo IdeaRepo;

        public IdeaService(IIdeaRepo ideaRepo)
        {
            IdeaRepo = ideaRepo;
        }

        public ICollection<Idea> FindAll()
        {
            return IdeaRepo.FindAll();
        }

        public ICollection<Idea> FindAllByUser(int userId, int pageIndex, int pageSize)
        {
            var ideas = IdeaRepo.FindAllByUser(userId, pageIndex, pageSize);
            foreach (var idea in ideas)
            {
                foreach (var picture in idea.Pictures)
                {
                    try
                    {
                        using (Image image = Image.FromFile(picture.PicturePath))
                        {
                            using (MemoryStream m = new MemoryStream())
                            {
                                image.Save(m, image.RawFormat);
                                byte[] imageBytes = m.ToArray();

                                // Convert byte[] to Base64 String
                                string base64String = Convert.ToBase64String(imageBytes);
                                picture.PicturePath = Zipper.Zip(base64String);
                            }
                        }
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e);
                        picture.PicturePath = "";
                    }
                }
            }
            return ideas;
        }

        public ICollection<Idea> FindAllLikedByUser(int userId, int pageIndex, int pageSize)
        {
            var ideas = IdeaRepo.FindAllLikedByUser(userId, pageIndex, pageSize);
            foreach (var idea in ideas)
            {
                foreach (var picture in idea.Pictures)
                {
                    try
                    {
                        using (Image image = Image.FromFile(picture.PicturePath))
                        {
                            using (MemoryStream m = new MemoryStream())
                            {
                                image.Save(m, image.RawFormat);
                                byte[] imageBytes = m.ToArray();

                                // Convert byte[] to Base64 String
                                string base64String = Convert.ToBase64String(imageBytes);
                                picture.PicturePath = Zipper.Zip(base64String);
                            }
                        }
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e);
                        picture.PicturePath = "";
                    }
                }
            }
            return ideas;
        }

        public ICollection<Idea> FindAllFromUser(int userId, int pageIndex, int pageSize)
        {
            var ideas = IdeaRepo.FindAllFromUser(userId, pageIndex, pageSize);
            foreach (var idea in ideas)
            {
                foreach (var picture in idea.Pictures)
                {
                    try
                    {
                        using (Image image = Image.FromFile(picture.PicturePath))
                        {
                            using (MemoryStream m = new MemoryStream())
                            {
                                image.Save(m, image.RawFormat);
                                byte[] imageBytes = m.ToArray();

                                // Convert byte[] to Base64 String
                                string base64String = Convert.ToBase64String(imageBytes);
                                picture.PicturePath = Zipper.Zip(base64String);
                            }
                        }
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e);
                        picture.PicturePath = "";
                    }
                }
            }
            return ideas;

        }

        public ICollection<Idea> FindAllByTag(string tagName, int pageIndex, int pageSize)
        {
            var ideas = IdeaRepo.FindAllByTag(tagName, pageIndex, pageSize);
            foreach (var idea in ideas)
            {
                foreach (var picture in idea.Pictures)
                {
                    try
                    {
                        using (Image image = Image.FromFile(picture.PicturePath))
                        {
                            using (MemoryStream m = new MemoryStream())
                            {
                                image.Save(m, image.RawFormat);
                                byte[] imageBytes = m.ToArray();

                                // Convert byte[] to Base64 String
                                string base64String = Convert.ToBase64String(imageBytes);
                                picture.PicturePath = Zipper.Zip(base64String);
                            }
                        }
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e);
                        picture.PicturePath = "";
                    }
                }
            }
            return ideas;
        }

        public Idea FindOne(int id)
        {
            return IdeaRepo.FindOne(id);
        }

        public void Save(Idea idea)
        {
            if(idea.Pictures != null)
            { 
            foreach (var ideaPicture in idea.Pictures)
            {
                int picId =  IdeaRepo.HighestPictureId() + 1;
                int ideaId = IdeaRepo.HighestIdeaId() + 1;
                

                    byte[] bytes = Convert.FromBase64String(Zipper.Unzip(ideaPicture.PicturePath));
                using (Image image = Image.FromStream(new MemoryStream(bytes)))
                {
                    
                    image.Save($"C:\\temp\\uploads\\Idea{ideaId}_PicId{picId}.jpeg", ImageFormat.Jpeg);
                }
                ideaPicture.PicturePath = $"C:\\temp\\uploads\\Idea{ideaId}_PicId{picId}.jpeg";
            }
            }
            IdeaRepo.Save(idea);
        }

        public void Update(int id, Idea idea)
        {
            IdeaRepo.Update(id, idea);
        }

        public void Edit(int id, Idea idea)
        {
            if (idea.Pictures != null)
            {
                int highestPicId = IdeaRepo.HighestPictureId();
                foreach (var ideaPicture in idea.Pictures)
                {
                    int picId = ideaPicture.PictureId;
                    if (picId == 0)
                    {
                        picId = highestPicId + 1;
                        highestPicId += 1;
                    }

                    byte[] bytes = Convert.FromBase64String(Zipper.Unzip(ideaPicture.PicturePath));
                    using (Image image = Image.FromStream(new MemoryStream(bytes)))
                    {

                        image.Save($"C:\\temp\\uploads\\Idea{id}_PicId{picId}.jpeg", ImageFormat.Jpeg);
                    }

                    ideaPicture.IdeaId = id;
                    ideaPicture.PicturePath = $"C:\\temp\\uploads\\Idea{id}_PicId{picId}.jpeg";
                }
            }
            IdeaRepo.Edit(id, idea);
        }
    }
}