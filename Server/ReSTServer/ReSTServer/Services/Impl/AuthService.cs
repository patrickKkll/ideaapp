﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReSTServer.Models;
using ReSTServer.Persistence;

namespace ReSTServer.Services.Impl
{
    public class AuthService : IAuthService
    {
        private IUserCredentialsRepo userCredentialsRepo;

        public AuthService(IUserCredentialsRepo userCredentialsRepo)
        {
            this.userCredentialsRepo = userCredentialsRepo;
        }

        public bool Authenticate(string user, string pwd)
        {
            return userCredentialsRepo.Authenticate(user, pwd);
        }

        public User LogIn(string user, string pwd)
        {
            return userCredentialsRepo.LogIn(user, pwd);
        }
    }
}