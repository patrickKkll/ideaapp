﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.UI;
using ReSTServer.Models;
using ReSTServer.Persistence.Impl;
using ReSTServer.Services;
using ReSTServer.Services.Impl;

namespace ReSTServer.Controllers
{
    [RoutePrefix("tidearApp/user")]
    public class UserController : BaseApiController
    {
        private IUserService userService;

        public UserController()
        {
            this.userService = new UserService(new UserRepo());
        }

        // GET: api/User
        [HttpGet]
        [BasicAuthentication]
        public HttpResponseMessage Get()
        {
            return base.BuildSuccessResult(HttpStatusCode.OK, userService.FindAll());
        }

        // GET: api/User/5
        [HttpGet]
        [BasicAuthentication]
        public HttpResponseMessage Get(int id)
        {
            return base.BuildSuccessResult(HttpStatusCode.OK, userService.FindOne(id));
        }

        
        [HttpGet]
        [Route("ideaIdForSuperlike={ideaId}")]
        [BasicAuthentication]
        public HttpResponseMessage GetForSuperlike(int ideaId)
        {
            return base.BuildSuccessResult(HttpStatusCode.OK, userService.GetForSuperlike(ideaId));
        }

        // POST: api/User
        [BasicAuthentication]
        public HttpResponseMessage Post([FromBody]User user)
        {
            userService.Save(user);
            return base.BuildSuccessResult(HttpStatusCode.OK);
        }

        // PUT: api/User/5
        [BasicAuthentication]
        public HttpResponseMessage Put(int id, [FromBody]User user)
        {
            userService.Update(id, user);
            return base.BuildSuccessResult(HttpStatusCode.OK);
        }

        // DELETE: api/User/5
        [BasicAuthentication]
        public HttpResponseMessage Delete(int id)
        {
            return base.BuildErrorResult(HttpStatusCode.BadRequest, HttpStatusCode.BadRequest.ToString(), "Deleting of User is not supported");
        }
    }
}
