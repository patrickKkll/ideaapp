﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ReSTServer.Models;
using ReSTServer.Persistence.Impl;
using ReSTServer.Services;
using ReSTServer.Services.Impl;

namespace ReSTServer.Controllers
{
    public class AuthController : BaseApiController
    {
        private IAuthService authService;

        public AuthController()
        {
            authService = new AuthService(new UserCredentialsRepo());
        }

        // GET: api/Auth
        public HttpResponseMessage Get()
        {
            return base.BuildErrorResult(HttpStatusCode.BadRequest, HttpStatusCode.BadRequest.ToString(), "Not supported");
        }

        // GET: api/Auth/5
        public HttpResponseMessage Get(int id)
        {
            return base.BuildErrorResult(HttpStatusCode.BadRequest, HttpStatusCode.BadRequest.ToString(), "Not supported");
        }

        // POST: api/Auth
        public HttpResponseMessage Post([FromBody]UserCredentials userCredentials)
        {
            var data = authService.LogIn(userCredentials.UserName, userCredentials.Password);
            if (data == null)
            {
                return base.BuildErrorResult(HttpStatusCode.Unauthorized, HttpStatusCode.BadRequest.ToString(),
                    "Wrong LogIn Info");
            }
            return base.BuildSuccessResult(HttpStatusCode.OK, data );

        }

        // PUT: api/Auth/5
        public HttpResponseMessage Put(int id, [FromBody]string value)
        {
            return base.BuildErrorResult(HttpStatusCode.BadRequest, HttpStatusCode.BadRequest.ToString(), "Not supported");
        }

        // DELETE: api/Auth/5
        public HttpResponseMessage Delete(int id)
        {
            return base.BuildErrorResult(HttpStatusCode.BadRequest, HttpStatusCode.BadRequest.ToString(), "Not supported");
        }
    }
}
