﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using ReSTServer.Services;
using ReSTServer.Models;
using ReSTServer.Persistence.Impl;

namespace ReSTServer.Controllers
{
    [RoutePrefix("tidearApp/idea")]
    public class IdeaController : BaseApiController
    {
        private IIdeaService ideaService;

        public IdeaController()
        {
            this.ideaService = new IdeaService(new IdeaRepo()); 
        }

        // GET: api/Idea
        [HttpGet]
        [BasicAuthentication]
        public HttpResponseMessage Get()
        {
            return base.BuildSuccessResult(HttpStatusCode.OK, ideaService.FindAll());
        }

        // GET: api/Idea/5
        [HttpGet]
        [BasicAuthentication]
        public HttpResponseMessage Get(int id)
        {
            return base.BuildSuccessResult(HttpStatusCode.OK, ideaService.FindOne(id));
        }

        [HttpGet]
        [Route("user={id}")]
        [BasicAuthentication]
        public HttpResponseMessage GetByUser(int id)
        {
            var headers = Request.Headers;
            int pageIndex = 1;
            int pageSize = 15;
            if (headers.Contains("PageIndex") && headers.Contains("PageSize"))
            {
                pageIndex = Convert.ToInt32(headers.GetValues("PageIndex").First());
                pageSize = Convert.ToInt32(headers.GetValues("PageSize").First());
            }

            return base.BuildSuccessResult(HttpStatusCode.OK, ideaService.FindAllByUser(id, pageIndex, pageSize));
        }

        [HttpGet]
        [Route("myIdeas/{id}")]
        [BasicAuthentication]
        public HttpResponseMessage GetMyIdeas(int id)
        {
            var headers = Request.Headers;
            int pageIndex = 1;
            int pageSize = 15;
            if (headers.Contains("PageIndex") && headers.Contains("PageSize"))
            {
                pageIndex = Convert.ToInt32(headers.GetValues("PageIndex").First());
                pageSize = Convert.ToInt32(headers.GetValues("PageSize").First());
            }

            return base.BuildSuccessResult(HttpStatusCode.OK, ideaService.FindAllFromUser(id, pageIndex, pageSize));
        }


        [Route("likedIdeas/{id}")]
        [BasicAuthentication]
        public HttpResponseMessage GetLikedIdeas(int id)
        {
            var headers = Request.Headers;
            int pageIndex = 1;
            int pageSize = 15;
            if (headers.Contains("PageIndex") && headers.Contains("PageSize"))
            {
                pageIndex = Convert.ToInt32(headers.GetValues("PageIndex").First());
                pageSize = Convert.ToInt32(headers.GetValues("PageSize").First());
            }

            return base.BuildSuccessResult(HttpStatusCode.OK, ideaService.FindAllLikedByUser(id, pageIndex, pageSize));
        }

       
        [Route("ideaTags/{tagName}")]
        [BasicAuthentication]
        public HttpResponseMessage GetIdeasByTag(string tagName)
        {
            var headers = Request.Headers;
            int pageIndex = 1;
            int pageSize = 15;
            if (headers.Contains("PageIndex") && headers.Contains("PageSize"))
            {
                pageIndex = Convert.ToInt32(headers.GetValues("PageIndex").First());
                pageSize = Convert.ToInt32(headers.GetValues("PageSize").First());
            }

            return base.BuildSuccessResult(HttpStatusCode.OK, ideaService.FindAllByTag(tagName, pageIndex, pageSize));
        }
        // POST: api/Idea
        [HttpPost]
        [BasicAuthentication]
        public HttpResponseMessage Post(Idea idea)
        {
            ideaService.Save(idea);
            return base.BuildSuccessResult(HttpStatusCode.OK);
        }

        // PUT: api/Idea/5
        [BasicAuthentication]
        public HttpResponseMessage Put(int id, [FromBody] Idea idea)
        {
           ideaService.Update(id, idea);
            return base.BuildSuccessResult(HttpStatusCode.OK);
        }

        [HttpPut]
        [Route("myIdeas/{id}")]
        [BasicAuthentication]
        public HttpResponseMessage EditMyIdea(int id, [FromBody] Idea idea)
        {
            ideaService.Edit(id, idea);
            return base.BuildSuccessResult(HttpStatusCode.OK);
        }

        // DELETE: api/Idea/5
        [BasicAuthentication]
        public HttpResponseMessage Delete(int id)
        {
            return base.BuildErrorResult(HttpStatusCode.BadRequest, HttpStatusCode.BadRequest.ToString(), "Deleting of Idea is not supported");
        }
    }
}
