﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace ReSTServer.Models
{
	public class Idea
	{
        [Key]
        public int IdeaId { get; set; }

        public int UserId { get; set; }
	    [JsonIgnore]
        public User User { get; set; }

        public string Title { get; set; }

        public string Summary { get; set; }

        public int Likes { get; set; }

	    public int Superlikes { get; set; }

        public int Dislikes { get; set; }

        public DateTime CreationDate { get; set; }

        public string CreationPlace { get; set; }

        public bool IsAccepted { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<IdeaMemo> Memos { get; set; }

	    public ICollection<IdeaPicture> Pictures { get; set; }

        public ICollection<UserIdeaInteraction> Interactions { get; set; }
        
	    public ICollection<Tag> Tags { get; set; }
    }
}