﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace ReSTServer.Models
{
    public class UserIdeaInteraction
    {
        [Key]
        public int InteractionId { get; set; }

        public TimeSpan TimeSpent { get; set; }

        public DateTime InteractionDate { get; set; }

        public bool Liked { get; set; }

        public bool SuperLike { get; set; }

        public string PlaceViewed { get; set; }

        public int UserId { get; set; }
        [JsonIgnore]
        public User User { get; set; }

        public int IdeaId { get; set; }
        [JsonIgnore]
        public Idea Idea { get; set; }
    }
}