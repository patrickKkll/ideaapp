﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ReSTServer.Models
{
    public class UserCredentials
    {
        [ForeignKey("User")]
        public int UserCredentialsId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        [JsonIgnore]
        public byte[] Salt { get; set; }

        #region NavigationProperties
        public virtual User User { get; set; }
        #endregion
    }
}