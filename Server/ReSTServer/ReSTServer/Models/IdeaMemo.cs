﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace ReSTServer.Models
{
    public class IdeaMemo
    {
        [Key]
        public int MemoId { get; set; }

        public int IdeaId { get; set; }
        [JsonIgnore]
        public Idea Idea { get; set; }

        public string MemoPath { get; set; }

    }
}