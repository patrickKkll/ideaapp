﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Web;

namespace ReSTServer.Models
{
    public class Tag
    {
        [Key]
        public int TagId { get; set; }

        public string Name { get; set; }

        [JsonIgnore]
        public ICollection<Idea> Ideas { get; set; }

    }
}