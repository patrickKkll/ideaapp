﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace ReSTServer.Models
{
    public class User
    {
        public User()
        {
            this.UserIdeaInteractions = new System.Collections.Generic.List<UserIdeaInteraction>();
            this.Ideas = new System.Collections.Generic.List<Idea>();
        }

        [Key]
        public int UserId { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public Gender Gender { get; set; }

        public string Department { get; set; }

        public UserRights UserRights { get; set; }

        public  bool IsDeleted { get; set; }

        public ICollection<UserIdeaInteraction> UserIdeaInteractions { get; set; }
        
        public ICollection<Idea> Ideas { get; set; }
        
        #region NavigationProperties
        [JsonIgnore]
        public UserCredentials UserCredentials { get; set; }
        #endregion
    }

    public enum Gender
    {
        Female,
        Male
    }

    public enum UserRights
    {
        User,
        Admin
    }
}